# Compte rendu jour 1
**Lien du vers le pad https://pad.actilis.net/p/cs2ip16-Bruz**
**Lien vers le cour https://cl.actilis.net/index.php/s/FXQqcMdR4pPSs5B**
## Swap ( à finir)
fsblk 
swapon 

Copie de /home
 ```
 cp -r /home /savehome
 
 ```
 /!\ Pour garder les droits d'utilisateurs des dossier \
 Création d'une archive :
 tar -czf **nom_archive nom_repertoire** \
Option utile tar :\
c -> creation \
x -> extraction \
v -> verbose (Il parle) \
f -> indiquer le nom du ficher \


mkfs 
make files system pour monter un system de fichier ici on vas le monter dans le /home
```
sudo mkfs.ext4 /dev/sdb2

mke2fs 1.46.2 (28-Feb-2021)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 391e2d21-d890-4758-9c84-bbbb2821ad33
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

ensuite il faut le monter au lancement du systeme dans le fichier */etc/fstab*
```
<file system>                         <mount point>   <type> <options>       <dump>  <pass>
UUID=391e2d21-d890-4758-9c84-bbbb2821ad33       /home   ext4    defaults

```
Puis tester le fichier fstab 
```
mount -av
/                         : ignoré
/boot                     : déjà monté
none                      : ignoré
/media/cdrom0             : ignoré
none                      : ignoré
/home                    : successfully mounted
```
On veut verifier que notre point de montage est bien monté
```
lsblk
NAME             MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                8:0    0   20G  0 disk
├─sda1             8:1    0  487M  0 part /boot
├─sda2             8:2    0  954M  0 part [SWAP]
└─sda3             8:3    0 18,6G  0 part
  └─rootvg-slash 254:0    0   15G  0 lvm  /
sdb                8:16   0   10G  0 disk
├─sdb1             8:17   0    1G  0 part [SWAP]
├─sdb2             8:18   0    1G  0 part /home
├─sdb3             8:19   0    1G  0 part
├─sdb4             8:20   0    1G  0 part
├─sdb5             8:21   0    1G  0 part
├─sdb6             8:22   0    1G  0 part
├─sdb7             8:23   0    1G  0 part
└─sdb8             8:24   0    1G  0 part
sr0               11:0    1  378M  0 rom
```

On ajoute notre savegarde de home dans le nouveau point de montage
```
tar -xvf homesave.tar
```
## LVM
Les outils lvm ne sont pas installer de base sur debian *apt install lvm2* \
la commande pv permet la gestion des volumes physique *physcal volume*
option pratique
pvcreate -> créer un volume physique *(asser logique)*\
pvscan -> Liste les volumes présent\
pvremove -> *(toujour logique fin non physique)*

Un volume groupe permet de créer un group de volume physique (*VG Volum group*)

Pour plus d'info 
```
vgdisplay 
  --- Volume group ---
  VG Name               vgdata
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               1020,00 MiB
  PE Size               4,00 MiB
  Total PE              255
  Alloc PE / Size       0 / 0
  Free  PE / Size       255 / 1020,00 MiB
  VG UUID               XMXjxC-Y52h-Kljc-Nxkt-g10g-kEx6-2sswCR

  --- Volume group ---
  VG Name               rootvg
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  4
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                1
  Open LV               1
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <18,59 GiB
  PE Size               4,00 MiB
  Total PE              4759
  Alloc PE / Size       3840 / 15,00 GiB
  Free  PE / Size       919 / <3,59 GiB
  VG UUID               FcgrBe-jqOD-WJEb-IwvQ-K16p-Yq4a-BZpzQt
```
On peut voir que l'unitée d'allocation minimal est 4 mio

Creation d'un volume logique permet l'utilisation des groupes de volume 
```
sudo lvcreate --name vgdata/lvdata --size 500M
```
Ici on créer un goupe de volume du nom de lgdata avec le groupe logique vgdata de taille de 500MIO

lvresize permet de changer la taille du groupe de volume 

Montage d'un volume logique
```
sudo mkfs.ext4 /dev/vgdata/lvdata
```
Recupérer l'UUID 
```
sudo lsblk -f
```
Création du fichier reception du point de montage
```
sudo mkdir /data
```
Pour créer le lien a chaque demarage de la machine il faut aller dans */etc/fstab*
```
# <file system>                         <mount point>   <type>  <options>       <dump>  <pass>
/dev/vgdata/lvdata                      /data           ext4    defaults   
```
Maintenant on monte le tout avec *mount -av* \
a pour all (Il vas donc lire dans le fichier fstab) et v pour le mode verbos (*bavard*)
```
/                         : ignoré
/boot                     : déjà monté
none                      : ignoré
/media/cdrom0             : ignoré
none                      : ignoré
/home                     : déjà monté
/data                    : successfully mounted
```
L'utisateur a besoin de plus de place pour ses données 
```
sudo lvresize /dev/vgdata/lvdata --size 550M
```
OU
```
sudo lvresize /dev/vgdata/lvdata --size +50M
```
Mais il faut aussi l'affecter 
```
sudo resize2fs /dev/vgdata/lvdata
```
Si on lespace que l'on veux alouer nest pas disponible dans le groupe de volume
```
sudo lvresize /dev/vgdata/lvdata --size +1G
Insufficient free space: 256 extents needed, but only 117 available

sudo vgextend vgdata /dev/sdb4
Volume group "vgdata" successfully extended

sudo lvresize /dev/vgdata/lvdata --size +1G
Size of logical volume vgdata/lvdata changed from 552,00 MiB (138 extents)
to <1,54 GiB (394 extents).
Logical volume vgdata/lvdata successfully resized.
```
Ici on a ajouter le volume physique sdb4 qui a été pationner au préalable en lvm