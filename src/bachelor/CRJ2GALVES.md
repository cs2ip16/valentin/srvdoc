# Compte rendu jour 2
**Lien du vers le pad https://pad.actilis.net/p/cs2ip16-Bruz**
**Lien vers le cour https://cl.actilis.net/index.php/s/FXQqcMdR4pPSs5B**

## Prérquis ansible 
- privilege sur les machine cible 
- clé ssh dans authorized_keys
- nopasswd dans /etc/sudoers (plus d'info plus bas)

Démontage du péripherique /data

```
umount {Chemin répertoir monté}
umount /data
```
On surprime les groupe de partition que l'on a créé

```
sudo lvremove vgdata/lvdata
sudo vgremove 
```
## Raid
*Processus de gestion d'agrégation de données*

Pour gérer le raid logiciel on installe le packet mdadm 
Ici on le fait avec ansible 
```
sudo ansible localhost -m package -a "name=mdadm state=latest"
```
Creation du premier périphérique 

```
sudo mdadm --create /dev/md0 --level 1 --raid-devices 2 /dev/sdb6 missing
```
/dev/md0 -> grap créée \
--level 1 -> niveau de raid *RAID 1 ici* \
--raid-devices 2 -> nb de partition utilisés \
/dev/sdb6 -> chemin du partition \
missing -> il manque un disque \

Pour monitorer
```
sudo watch -n 1 mdadm --detail /dev/md0
```
import de la deuxieme partition
```
sudo mdadm --manage /dev/md0 --add /dev/sdb7
```
Le raid est une entité il faut donc formater au niveau de la grappe 
```
sudo mkfs.ext4 /dev/md0
sudo mount /dev/md0 /data 
lsblk
NAME             MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
sda                8:0    0   20G  0 disk
├─sda1             8:1    0  487M  0 part  /boot
├─sda2             8:2    0  954M  0 part  [SWAP]
└─sda3             8:3    0 18,6G  0 part
  └─rootvg-slash 254:0    0   15G  0 lvm   /
sdb                8:16   0   10G  0 disk
├─sdb1             8:17   0    1G  0 part  [SWAP]
├─sdb2             8:18   0    1G  0 part  /home
├─sdb3             8:19   0    1G  0 part
├─sdb4             8:20   0    1G  0 part
├─sdb5             8:21   0    1G  0 part
├─sdb6             8:22   0    1G  0 part
│ └─md0            9:0    0 1022M  0 raid1
├─sdb7             8:23   0    1G  0 part
│ └─md0            9:0    0 1022M  0 raid1
└─sdb8             8:24   0    1G  0 part
  └─md0            9:0    0 1022M  0 raid1
sr0               11:0    1  378M  0 rom
vgalves@debianval:~$ mount /dev/md0 /data
mount: /data: seul le superutilisateur peut utiliser mount.
vgalves@debianval:~$ sud omount /dev/md0 /data
-bash: sud : commande introuvable
vgalves@debianval:~$ sudo mount /dev/md0 /data
vgalves@debianval:~$ lsblk
NAME             MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
sda                8:0    0   20G  0 disk
├─sda1             8:1    0  487M  0 part  /boot
├─sda2             8:2    0  954M  0 part  [SWAP]
└─sda3             8:3    0 18,6G  0 part
  └─rootvg-slash 254:0    0   15G  0 lvm   /
sdb                8:16   0   10G  0 disk
├─sdb1             8:17   0    1G  0 part  [SWAP]
├─sdb2             8:18   0    1G  0 part  /home
├─sdb3             8:19   0    1G  0 part
├─sdb4             8:20   0    1G  0 part
├─sdb5             8:21   0    1G  0 part
├─sdb6             8:22   0    1G  0 part
│ └─md0            9:0    0 1022M  0 raid1 /data
├─sdb7             8:23   0    1G  0 part
│ └─md0            9:0    0 1022M  0 raid1 /data
└─sdb8             8:24   0    1G  0 part
  └─md0            9:0    0 1022M  0 raid1 /data
sr0               11:0    1  378M  0 rom
```
ou avec ansible
```
ansible-doc ansible.posix.mount
sudo ansible localhost -m ansible.posix.mount -a "src=/dev/md0 path=/data state=mounted fstype=ext4"
```

Au reboot le dev md0 se renome md123
donc 
```
sudo ansible localhost -m ansible.posix.mount -a "src=/dev/md0 path=/data state=absent fstype=ext4"

sudo ansible localhost -m ansible.posix.mount -a "src=UUID=f247e8b3-89ed-4d45-9173-2c82ca89b3e4 path=/data state=mounted fstype=ext4"
```
Pour recuperer le UUID  
lsblk -f

Avec ansi l'argument opts permet de passer un argument directement a la commande executé par ansible
ansible localhost -m filesystem -a "state=present dev=/dev/md0 fstype=ext4 force=yes opts='-U cf32606b-a724-41e3-b263-11f51e31d727'"

Générer une nouvel paire de clés ssh

```
ssh-keygen
```

Le déployer vers la machine cible (Ici la meme machine) 
```
ssh-id-copy 127.0.0.1
```

```
# Tete de chapitre, 
# on a un nom
# on associe des cibles à des taches
- name: chapitre1
  hosts:
  - all

  tasks:
  # chaque tâche a un nom,
  # utilise un module
  #   dont on cite les paramètres
  - name: ma petite action
    package:
      name: uuid-runtime
      state: latest

  - name: ma seconde petite action
    nom_de_module:
      param1_du_module: savaleur
      param2_du_module: savaleur
```

**/!\ si ansible a besoin d'un mot de passe dans le fichier sudoers de la machine cible vérifier %sudo   ALL=(ALL:ALL) NOPASSWD :ALL** \

## Ficher créé
```
---
- name: chapitre 1
  hosts:
  - all
  become: true
  tasks:
  - name: install-uuid
    package:
      name: uuid-runtime
      state: latest

  - name: get-uuid
    command: uuidgen
    register: uuidgen_result

  - name: test
    debug:
      msg: "{{uuidgen_result.stdout}}"
  
  - name: umount
    mount:
      path: /data
      state: absent
  
  - name: deleteformat
    community.general.filesystem:
      state: absent
      dev: /dev/md0
  
  - name: mkformat
    community.general.filesystem:
      state: present
      dev: /dev/md0
      fstype: ext4
      opts: -U "{{uuidgen_result.stdout}}"

    
  - name: mount
    mount:
      path: /data
      src: UUID="{{uuidgen_result.stdout}}"
      fstype: ext4
      state: mounted 
```
Pour executer le playbook
```
ansible-playbook make-fillesystem.yml
```
## Mettre a jour la collection ansible
```
ansible-galaxy collection install <nom du collection ex: communty.general>, 
```

**/!\ /dev/md0 change de nom au reboot : /dev/md127** 