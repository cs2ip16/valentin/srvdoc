# CR Jour 4 Galves

**Lien du vers le pad https://pad.actilis.net/p/cs2ip16-Bruz**
**Lien vers le cour https://cl.actilis.net/index.php/s/FXQqcMdR4pPSs5B**


lsof : sert a voir qui lit dans le fichier pointé
-p : pour avoir les processus

## outils de monitoring

sysstat *a installer*

vmstat permet de voir lactivité de la machine 
bi :blocks in *lecture de blocs de stockage*
bo : block out *ecriture*
si : swap in *stock en swap pour liberer de la ram si il stock beaucoup = beosin ram*
so : swap out  


iostat *permet de voir les in/out des filsystem*
-o : permet de choisir le format de la sortie *JSON*

mpstat affiche lactivitées des processeur
-P : choisir le processeur visé *ALL pour tous*

pidstat voir ce que conssome les processus

pour enable systat mettre *true* dans /etc/default/sysstat

pour voir les ancienne stat sysstat

sudo sar -r -f *chemain du fichier* -s *start time* -e *end time*
ex :
```
sudo sar -r -f /var/log/sysstat/sa22 -s 16:00 -e 16:20
```

//Pour recupérer + d'info *lm-sensors*


# NETWORK

commande ip 
ip a des sous commande 
address
link
Mais cela sont des commandes en ponctuel et perdu au redemarage
Mais il existe network manager 
```
nmcli
```
**Ajout d'une connection **
avec nmcli connection
nmcli connection add ifname {device_name} ipv4.addresses {@ip}/{CIDR}  ipv4.gateway {@ip gateway} con-name {connection_name} type ethernet ipv4.method {manual/auto}
exemple 
```
nmcli connection add ifname ens38 ipv4.addresses 10.0.0.2/24  ipv4.gateway 10.0.0.254 con-name alamano type ethernet ipv4.method manual
```
Cette cnf se retrouve ensuite dans */etc/NetworkManager/{connection_name}*
Le ficher garde le nom a qu'on lui a donnée a la création
Network manager soccupe aussi des DNS et il regenere le resolv.conf avec les options que on lui a donner