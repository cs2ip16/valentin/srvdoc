# CR Jour 3 Galves

**Lien du vers le pad https://pad.actilis.net/p/cs2ip16-Bruz**
**Lien vers le cour https://cl.actilis.net/index.php/s/FXQqcMdR4pPSs5B**

Gestionnaire de demarage par defaut **GRUB**
Les base du démarage de la machine se trouve dans /init *c'est un lien symbolique*

La gestion des application au demarage avec **systemd**
les "Services" sont appeler des unité
Systemed soccupe aussi des motage au demarage
Les target sont des group d'unités

systemctl -> commande qui permet de gérer les unitées *(services)* au demarage ou a chaud
*get/set-defaut* permet de gerer l'environement lancer au demarage

### Creation d'une unité dans /etc/systemd/<nom_unitée> *Ici un point de montage pour /data*
```
[Unit]

[Mount]
What=UUID=14d45bb5-9bf4-4c94-8812-432edbba954c
Where=/data
Type=ext4
Options=defaults
```

Le systemd appel le fichier fstab au lancement donc on peut crée un service nous même pour monter /data 
*/etc/systemd/system/data.mount*
```
[Unit]
[Mount]
What=UUID=bd39f5f7-e16f-48a0-b1e0-b0c915866527
Where=/data
Type=ext4
Options=defaults
[Install]
WantedBy=multi-user.target
```
Ou automatisable avec ansible ;)

'''
---
---
- name: chapitre 1
  hosts:
  - all
  become: true
  tasks:

  - name: install-uuid-genrator
    package:
      name: uuid-runtime
      state: latest
    tags: always
  
  - name: generate-uuid
    command: uuidgen
    register: uuidgen_result
    tags: always


  - name: test
    debug:
      msg: "{{uuidgen_result.stdout}}"  
    tags: debug

  - name: umount
    mount:
      path: /data
      state: unmounted  
    tags: always

  - name: deleteformat
    community.general.filesystem:
      state: absent
      dev: /dev/{{ raid_device }}  
    tags: always

  - name: mkformat
    community.general.filesystem:
      state: present
      dev: /dev/{{ raid_device }}
      fstype: ext4
      opts: -U "{{uuidgen_result.stdout}}"
    tags: always

  - name: mount
    mount:
      path: /data
      src: UUID="{{uuidgen_result.stdout}}"
      fstype: ext4
      state: mounted
    tags: fstab

  - name: "desactive/disable {{ mountpoint | regex_replace('^/','') }}.mount"
    ansible.builtin.systemd:
      name: "{{ mountpoint | regex_replace('^/','') }}.mount"
      state: stopped
      enabled: no
    tags: systemd

  - name: create mount file for systemd
    ansible.builtin.template:
      src: templates/mount.j2
      dest: "/etc/systemd/system{{ mountpoint }}.mount"
      owner: root
      group: root
      mode: 0644
    vars:
      device: "UUID={{uuidgen_result.stdout}}"
    tags: systemd
  
  - name: reload daemon
    ansible.builtin.systemd:
      daemon_reload: yes
    tags: systemd
  
  - name: "active/enable {{ mountpoint | regex_replace('^/','') }}.mount"
    ansible.builtin.systemd:
      name: "{{ mountpoint | regex_replace('^/','') }}.mount"
      state: started
      enabled: yes
    tags: systemd
```
le template permet de crée un ficher préfait par nos soins avec des variable defini dans ansible-inventory ou dans le playbook

Ensuite la création d'un service par ansible 
```
---
- name: make service
  hosts:
  - all
  become: true
  vars:
    service_name: compteur
  tasks:
    - name: depot_script
      copy:
        dest: /usr/local/bin/{{ service_name }}.sh
        src: Script/{{ service_name }}.sh
        owner: root
        group: root
        mode: 0710
    
    - name: dépot_service
      ansible.builtin.template:
        src: templates/service.j2
        dest: "/etc/systemd/system/{{ service_name }}.service"
        owner: root
        group: root
        mode: 0644
    
    - name: dépot_compteur
      ansible.builtin.template:
        src: templates/timer.j2
        dest: "/etc/systemd/system/{{ service_name }}.timer"
        owner: root
        group: root
        mode: 0644


    - name: reload daemon
      ansible.builtin.systemd:
        daemon_reload: yes

    - name: "active/enable {{ service_name }}.timer"
      ansible.builtin.systemd:
        name: "{{ service_name }}.timer"
        state: started
        enabled: true    

  
    - name: "active/enable {{ service_name }}.service"
      ansible.builtin.systemd:
        name: "{{ service_name }}.service"
        state: stopped
        enabled: false
```
