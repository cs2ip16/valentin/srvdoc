# CR Jour 4 Galves

**Lien du vers le pad https://pad.actilis.net/p/cs2ip16-Bruz**
**Lien vers le cour https://cl.actilis.net/index.php/s/FXQqcMdR4pPSs5B**

Déroulé du cour :
- monitoring des resource
- gestion des logs 

## **GESTION DES LOG**
*syslog est un protocole* 

Ancienement utilisé *syslog* aujourd'hui remplacer par systemd  
**Mais** syslog est encore utilisé aujourd'hui

Le nom du serveur interne à la machine est *rsyslog*
La configuration de trouve dans */etc/rsyslog.d/*.conf*
Ici on peut definir le comportement de la gestion des logs
**Des informations plutôt intéressant son presentes dans les pages 200 du support**
On peut voir que syslog ecrit dans plusieurs fichiers 
```
/var/log/mail.err:Jun 22 09:57:27 debianval sontag[paslemien]: Message à caractere informatif
/var/log/mail.err:Jun 22 09:57:38 debianval sontag[paslemien]: Message à caractere informatif
/var/log/mail.info:Jun 22 09:57:27 debianval sontag[paslemien]: Message à caractere informatif
/var/log/mail.info:Jun 22 09:57:38 debianval sontag[paslemien]: Message à caractere informatif
/var/log/mail.log:Jun 22 09:57:27 debianval sontag[paslemien]: Message à caractere informatif
/var/log/mail.log:Jun 22 09:57:38 debianval sontag[paslemien]: Message à caractere informatif
/var/log/mail.warn:Jun 22 09:57:27 debianval sontag[paslemien]: Message à caractere informatif
/var/log/mail.warn:Jun 22 09:57:38 debianval sontag[paslemien]: Message à caractere informatif
grep: /var/log/private: est un dossier
grep: /var/log/runit: est un dossier
/var/log/syslog:Jun 22 09:57:27 debianval sontag[paslemien]: Message à caractere informatif
/var/log/syslog:Jun 22 09:57:38 debianval sontag[paslemien]: Message à caractere informatif
```
- mail.warn
- mail.info
- mail.err
- syslog

Configuration d'un redirecteur vers un serveur de collecte de log avec *rsyslog-relp* 
Dans */etc/rsyslog.conf*
```
module(load="omrelp") 
local1.info                        :omrelp:192.168.13.69:2514
```

## Avec ansible 
Utilisation des rôles
Création de role ansible 
```
mkdir roles/rsyslog
ansible-galaxy role init roles/rsyslog/
```
donc dans le playbook on appel juste le role 
```
---
- name: logs
  hosts: all
  become: true
  vars:
    remote_ip : 192.168.13.69
    remote_port : 2514
  roles:
  - rsyslog
```
grace a ca on peux appler plusieurs roles avec le meme playbook 
un role fait une action composer de plusieurs taches 
Cela permet aussi de partitionner les blocks du playbook dans des repertoir et fichier different qui permet de rendre la lecture et donc l'ecriture

## les handler 
les handler servent a lancer une action **SI** la tache se conclue par un changement d'etat
pour cela il faut faire l'appel du handler avec *notify*
```
  ansible.builtin.template:
    src: rsyslog-relp.j2
    dest: /etc/rsyslog.d/rsyslog-relp.conf
    owner: root
    group: root
    mode: 0644
  notify:
   - reload service
```
et faire son handler
*Le handler est une tache*
```
---
# handlers file for roles/rsyslog/
- name: reload service
  ansible.builtin.systemd:
    name: rsyslog
    state: restarted
```


