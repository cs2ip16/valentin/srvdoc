# Packaging
**Jour 2**

### **Plan de la journée :**
-----
 - Manipuler rpm - yum
 - Compiler un logiciel libre (a partir de ses sources)
 - Créer un pakcage RPM
 - Créer un dépôt pour mettre à dispo mes packages
 - Déclarer mon dépôt sur les pc clients

## RPM
!!! info "DEF RPM"

	**RPM Package Manager**, ou plus simplement RPM, est un système de gestion de paquets de logiciels utilisé sur certaines distributions GNU/Linux. Le système est composé d'un format ouvert et d'un logiciel libre de manipulation des fichiers de ce format. C'est le format utilisé par Linux Standard Base. *Équivalant dpkg sur la famille ubuntu*

*Avec rpm on peut voir la version d'un paquet, ses besoins , ses prérequis, son instalation*
``` bash linenums="1"
[vgalves@stream9-1-AG ~]$ rpm -qf /usr/sbin/rsyslogd 
rsyslog-8.2102.0-105.el9.x86_64
[vgalves@stream9-1-AG ~]$ rpm -qf /usr/sbin/rsyslogd --requires 
/bin/sh
/bin/sh
/bin/sh
bash >= 2.0
config(rsyslog) = 8.2102.0-105.el9
libc.so.6()(64bit)
libc.so.6(GLIBC_2.12)(64bit)
libc.so.6(GLIBC_2.14)(64bit)
libc.so.6(GLIBC_2.15)(64bit)
libc.so.6(GLIBC_2.17)(64bit)
libc.so.6(GLIBC_2.2.5)(64bit)
libc.so.6(GLIBC_2.27)(64bit)
libc.so.6(GLIBC_2.3)(64bit)
libc.so.6(GLIBC_2.3.2)(64bit)
...
[vgalves@stream9-1-AG ~]$ rpm -qf /usr/sbin/rsyslogd --provides 
config(rsyslog) = 8.2102.0-105.el9
rsyslog = 8.2102.0-105.el9
rsyslog(x86-64) = 8.2102.0-105.el9
syslog



[vgalves@stream9-1-AG ~]$ rpm -qf /usr/sbin/rsyslogd --scripts 
postinstall scriptlet (using /bin/sh):
for n in /var/log/{messages,secure,maillog,spooler}
do
        [ -f $n ] && continue
        umask 066 && touch $n
done


if [ $1 -eq 1 ] && [ -x "/usr/lib/systemd/systemd-update-helper" ]; then
    # Initial installation
    /usr/lib/systemd/systemd-update-helper install-system-units rsyslog.service || :
fi
preuninstall scriptlet (using /bin/sh):


if [ $1 -eq 0 ] && [ -x "/usr/lib/systemd/systemd-update-helper" ]; then
    # Package removal, not upgrade
    /usr/lib/systemd/systemd-update-helper remove-system-units rsyslog.service || :
fi
postuninstall scriptlet (using /bin/sh):


if [ $1 -ge 1 ] && [ -x "/usr/lib/systemd/systemd-update-helper" ]; then
    # Package upgrade, not uninstall
    /usr/lib/systemd/systemd-update-helper mark-restart-system-units rsyslog.service || :
fi
```

### YUM
!!! info "DEF Yum"

	**Yum**, *pour Yellowdog Updater Modified, est un gestionnaire de paquets pour des distributions Linux telles que Fedora, CentOS et Red Hat Enterprise Linux, créé par Yellow Dog Linux. Il permet de gérer l'installation et la mise à jour des logiciels installés sur une distribution*
	**Yum est aujourd'hui dépricier car tourne sous python 2.7 qui n'est pu supporter**
	Il est donc replacer aujourd'hui par **DNF**
	De plus DNF à apporter des ajouts 

*Équivalant famille ubuntu : apt*
```
[vgalves@stream9-1-AG ~]$ yum search nano 
Dernière vérification de l’expiration des métadonnées effectuée il y a 0:04:38 le ven. 02 déc. 2022 10:11:31.
============================================================================== Nom correspond exactement à : nano ===============================================================================
nano.x86_64 : A small text editor
```

La configuration de yum se trouve dans `/etc/yum.conf`
Les dépot sont présent dans `/etc/yum.repos.d/`
On peut lister les paquet installer et activé avec `yum reposlist all`
``` linenums="1"
id du dépôt                                                                         nom du dépôt                                                                                        état
appstream                                                                           CentOS Stream 9 - AppStream                                                                         activé   
appstream-debuginfo                                                                 CentOS Stream 9 - AppStream - Debug                                                                 désactivé
appstream-source                                                                    CentOS Stream 9 - AppStream - Source                                                                désactivé
baseos                                                                              CentOS Stream 9 - BaseOS                                                                            activé   
baseos-debuginfo                                                                    CentOS Stream 9 - BaseOS - Debug                                                                    désactivé
baseos-source                                                                       CentOS Stream 9 - BaseOS - Source                                                                   désactivé
crb                                                                                 CentOS Stream 9 - CRB                                                                               désactivé
crb-debuginfo                                                                       CentOS Stream 9 - CRB - Debug                                                                       désactivé
crb-source                                                                          CentOS Stream 9 - CRB - Source                                                                      désactivé
docker-ce-nightly                                                                   Docker CE Nightly - x86_64                                                                          désactivé
docker-ce-nightly-debuginfo                                                         Docker CE Nightly - Debuginfo x86_64                                                                désactivé
docker-ce-nightly-source                                                            Docker CE Nightly - Sources                                                                         désactivé
docker-ce-stable                                                                    Docker CE Stable - x86_64                                                                           activé   
docker-ce-stable-debuginfo                                                          Docker CE Stable - Debuginfo x86_64                                                                 désactivé
docker-ce-stable-source                                                             Docker CE Stable - Sources                                                                          désactivé
docker-ce-test                                                                      Docker CE Test - x86_64                                                                             désactivé
docker-ce-test-debuginfo                                                            Docker CE Test - Debuginfo x86_64                                                                   désactivé
docker-ce-test-source                                                               Docker CE Test - Sources                                                                            désactivé
extras-common                                                                       CentOS Stream 9 - Extras packages                                                                   activé   
extras-common-source                                                                CentOS Stream 9 - Extras packages - Source                                                          désactivé
highavailability                                                                    CentOS Stream 9 - HighAvailability                                                                  désactivé
highavailability-debuginfo                                                          CentOS Stream 9 - HighAvailability - Debug                                                          désactivé
highavailability-source                                                             CentOS Stream 9 - HighAvailability - Source                                                         désactivé
nfv                                                                                 CentOS Stream 9 - NFV                                                                               désactivé
nfv-debuginfo                                                                       CentOS Stream 9 - NFV - Debug                                                                       désactivé
nfv-source                                                                          CentOS Stream 9 - NFV - Source                                                                      désactivé
resilientstorage                                                                    CentOS Stream 9 - ResilientStorage                                                                  désactivé
resilientstorage-debuginfo                                                          CentOS Stream 9 - ResilientStorage - Debug                                                          désactivé
resilientstorage-source                                                             CentOS Stream 9 - ResilientStorage - Source                                                         désactivé
rt                                                                                  CentOS Stream 9 - RT                                                                                désactivé
rt-debuginfo                                                                        CentOS Stream 9 - RT - Debug                                                                        désactivé
rt-source                                                                           CentOS Stream 9 - RT - Source                                                                       désactivé
```
On peut voir ici le que repos HighAvailability est desactivé

si on veut installer `booth`
``` bash
sudo dnf install booth
Dernière vérification de l’expiration des métadonnées effectuée il y a 1:04:52 le ven. 02 déc. 2022 09:26:29.
Aucune correspondance pour le paramètre: booth
Erreur : Impossible de trouver une correspondance: booth
```

```
sudo yum install booth --enablerepo="highavailability"
```

Cela active le repo juste l'installation

## Création du paquet rsync
```
wget https://download.samba.org/pub/rsync/src/rsync-3.2.7.tar.gz
```
On a pas **wget**
``` 
yum install wget
```

apres on le dezip



on verifi la signature
on telecharge la signature du fichier

``` bash
wget https://download.samba.org/pub/rsync/src/rsync-3.2.7.tar.gz.asc
``` 
on copy l'identifiant

``` bash
cat rsync-3.2.7.tar.gz.asc
``` 
On recupere la clé public
``` bash
gpg --recv-keys 0048C8B026D4C96F0E589C2F6C859FB14B96A8C5
```
On verifi la clé avec celle que l'on a téléchargé
```bash
gpg --verify rsync-3.2.7.tar.gz.asc
```

on veux compiler rsync
**Mais on a pas de compilateur C**
```bash 
./configure --prefix=/opt/rsynctest
```

Dans le INSTALL.md (je ne lai pas vu)

Il nous faut GCC
```bash
Sudo dnf install -y GCC
```

Quand on veux lancer l'installation il nous dit qui manque openssl/md4.h 
Donc on le cherche 
```bash
yum provides "*/openssl/md4.h*
```

Et on l'instale 
```bash 
dnf install -y openssl-devel
```

*Raccourci* 
```bash 
yum install "*/openssl/md4.h"
```

Pour installer xxhash.h \
On se rend sur le site https://pkgs.org/ \
On voit qu'il est sur le depot crb
```bash 
yum install -y xxhash-devel --enablerepo="crb"
```

ici j'ai installer le mauvais paquet avant 
Pour annuler cette installation 
```bash
yum history 
```
Je note le numero de la commande 
```bash
yum history undo -y 11
```

Maintenant **ON AUTOMATISE**

## TP
[Lien-TP](https://cl.actilis.net/index.php/s/frydpMr6wT8CQGN?path=%2FTPs)

Avec la commande **rpmbuild**

!!! tip "on peux lui donner 3 types de fichier source"

	- t : Fichier **tar** 
	- b : un **SPECFILE**
	- rebuild : un **SOURCEPKG**

!!! tip "Ensuite on lui dit jusqu'a ou on veux qu'il aille"

	- i : Do the "%install" stage from the spec file (after doing the %prep and %build stages). This generally involves the equivalent of a "make install".
	- b : Build a binary package (after doing the %prep, %build, and %install stages).
	- c : Do the "%build" stage from the spec file (after doing the %prep stage). This generally involves the equivalent of a "make".
	- p : Executes the "%prep" stage from the spec file. Normally this involves unpacking the sources and applying any patches.

*Le source pkg permet d'installer que les source du paquet* \
Cela génere un dossier rpmbuild

```bash
rpmbuild -tb rsync-3.2.7.tar.gz
```

!!! note "*by fmicaux*"

	- Préparation : exécution des commandes et macros de la section %prep ("p")
	- Compilation : exécution des commandes et macros de la section %build ("c") (intègre p)
	- Installation : exécution des commandes et macros de la section %install ("i") (intègre p et c)

	*toute macro de la section "%files" est exécutée à ce moment là aussi
	Listing des fichiers : contrôle du contenu de la section %files ("l")*
	- Grâce aux options (p, l, c, i, puis b, s, ou a), l'exécution peut être stoppée à un point précis.
	**Lorsque ces étapes réussissent, on peut envisager de produire un package :**
	- Package binaire : "b" (intègre p, l, c, et i)
	- Package source uniquement : "s",
	- Packages "binary and source" (binaire et source) : "a" (intègre p, l, c, et i)

	Rpmbuild utilise une petite arborescence13 : un répertoire "rpmbuild" avec ce contenu :
	- SOURCES : contient les archives de sources, les patches, les fichiers d'icônes.
	- SPECS : contient les fichiers SPEC, décrivant les opérations à résliser pour construire.
	- BUILD : répertoire de travail, là où %prep est réalisé et où %build est réalisé.
	- BUILDROOT : répertoire (temporaire) où %install est réalisé et %files contrôlé (nettoyé après).
	- RPMS : contient les paquetages binaires générés.
	- SRPMS : contient les paquetages sources générés.
	13 Depuis RHEL 6, c'est en principe sur votre HOME que "rpmbuild" crée cette arborescence.

	Si on veux ajouter des commande au siens de notre package a l'installation il faut se rendre dans le ficher  *rpmbuild/SPECS/rsync.spec*

	Et ajouter des commande dans la balise ``%install`` \
	Si cela rajoute des fichiers l'indiquer dans la balise ``%files``

Pour build :
```bash
rpmbuild --bb rpmbuild/SPECS/rsync.spec
```
Ensuit si on veux installer le paquet que l'on vien de modifier 
```bash
sudo rpm -ivh rpmbuild/RPMS/x86_64/rsync-3.2.7-2.x86_64.rpm
```
Si le paquet est deja installer remplacer ``-i`` par ``-U``


## Création d'un paquet

Avec 
```bash
rpm-dev
```
on cree un fichier spec vide avec 
```bash
rpmdev-newspec --type minimal --output vide.spec
```

Que l'on modifie a notre gise

```ini
Name:           vide
Version:        1.0
Release:        1%{?dist}
Summary:        Paquet vide

License:        MABELLELICENCE
URL:            www.oui.fr

BuildRequires:  rpm-build
Requires:       bash > 3.0

%description
Paquet bonjour

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc/
#%make_install
echo "Salut les filles" > $RPM_BUILD_ROOT/etc/vide.conf

%files
#%license add-license-file-here
#%doc add-docs-here
%config(noreplace) /etc/vide.conf



%changelog
* Fri Dec 02 2022 vgalves
```
On veux le rendre disponible
## Creation du dépot (repo)
```bash
dnf install -y create-repo
```

et on lance un serveur web sur ce repo 
```bash
docker container run -d --name rpmsrv --publish 8001:80 --volume /home/vgalves/rpmbuild/RPMS:/usr/local/apache2/htdocs --restart always  httpd
```

Et ajouter ce repo à dans les repo de sa machine 

touch /etc/yum.repos.d/mondepot.repo

```ini
[mondepot]
name=Mon beau dépot
baseurl=http://192.168.239.135:8001/
gpgcheck=1
gpgkey = http://192.168.239.135:8001/repodata/vgalveskey
```

## Assigné un clé gpg à un package 

Tous d'abort j'ajoute le repo à la  création de la machine avec le [kickstart](ks.cfg)
```ini
install -d -m 644 -o root -g root /mnt/sysimage/etc/yum.repos.d/mondepot.repo
curl http://192.168.239.135:8001/mondepot.repo > /mnt/sysimage/etc/yum.repos.d/mondepot.repo
```
!!! warning 
    **Toutes ses étapes son a faire en tant que root**

On vas générer un clé gpg  
[Documentaion de redhat](https://access.redhat.com/documentation/fr-fr/red_hat_network_satellite/5.5/html/channel_management_guide/sect-red_hat_network_satellite-channel_management_guide-building_custom_packages-digital_signatures_for_rhn_packages#sect-Red_Hat_Network_Satellite-Channel_Management_Guide-Digital_Signatures_for_RHN_Packages-Generating_a_GnuPG_Keypair)


```bash
gpg --full-gen-key
gpg: agent_genkey failed: Pas de pinentry
Échec de génération de la clef : Pas de pinentry
```

!!! note
     Il faut installer l'agent pinentry 

```bash
yum install pinentry
```

Il nous demande :

	- le type > RSA
	- une taille > 2048
	- un nom
	- un email
	- une passphrase 

on export notre signature
```bash
gpg --export -a > vgalveskey
```
On l'importe dans rpm 
```bash
 gpg --import vgalveskey
```

Pour verifier 
```bash
rpm -q gpg-pubkey --qf '%{name}-%{version}-%{release} --> %{summary}\n'
```

Ensuite on crée le fichier ``~/.rpmmarcos``
```ini linenums="1"
%_signature gpg
%_gpg_name vgalves 
#%_signature = type de la signature  
#%_gpg_name = ID de la signature
#%__gpg_sign_cmd pour qu il soir verbeux quand on l execute
```

Ensuit on signe le paquet 

!!! note 
    nécessite le packet rpm-sign
```
rpm --addsign
```

!!! tip

	Pour vider son cache dnf 

``` linenums="1"
Téléchargement des paquets :
[MIRROR] vide-1.0-1.el9.x86_64.rpm: Downloading successful, but checksum doesn't match. Calculated: 1dd5d3149cab0ee7ff81b80dd652671cf45f0ba1579f50fc11012842435d6e18(sha256)  Expected: 446eab82d7f6172066d163fd0f93ced0dfb81b0a4a54a70804f669bcc401d578(sha256)
[MIRROR] vide-1.0-1.el9.x86_64.rpm: Downloading successful, but checksum doesn't match. Calculated: 1dd5d3149cab0ee7ff81b80dd652671cf45f0ba1579f50fc11012842435d6e18(sha256)  Expected: 446eab82d7f6172066d163fd0f93ced0dfb81b0a4a54a70804f669bcc401d578(sha256)
[MIRROR] vide-1.0-1.el9.x86_64.rpm: Downloading successful, but checksum doesn't match. Calculated: 1dd5d3149cab0ee7ff81b80dd652671cf45f0ba1579f50fc11012842435d6e18(sha256)  Expected: 446eab82d7f6172066d163fd0f93ced0dfb81b0a4a54a70804f669bcc401d578(sha256)
[MIRROR] vide-1.0-1.el9.x86_64.rpm: Downloading successful, but checksum doesn't match. Calculated: 1dd5d3149cab0ee7ff81b80dd652671cf45f0ba1579f50fc11012842435d6e18(sha256)  Expected: 446eab82d7f6172066d163fd0f93ced0dfb81b0a4a54a70804f669bcc401d578(sha256)
[FAILED] vide-1.0-1.el9.x86_64.rpm: No more mirrors to try - All mirrors were already tried without success                                                                                      

Les paquets téléchargés ont été mis en cache jusqu’à la prochaine transaction réussie.
Vous pouvez supprimer les paquets en cache en exécutant « dnf clean packages ».
Erreur : Erreur de téléchargement des paquets :
  vide-1.0-1.el9.x86_64: Cannot download, all mirrors were already tried without success
```