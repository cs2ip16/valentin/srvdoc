# Native Cloud 
# Jour 1 25/10 

# Ressources 
[Pad](https://notes.tanzu.fr/p16r)
[Git du cours](https://github.com/davoult/initiation-kubernetes)

Chroot : permet d'isoler un service ou un utilisateur dans un répertoire 

## Docker :
Docker à crée la **conteneurisation applicative**

Cela permet :
	- Immuabilité
	- Idempendence 
	- Méthode déclarative 

Qu'un seul processus doit tourné dans un conteneur 
 
 Par exemple une application web qui a apache et php doit avoir 2 conteneurs, cela permet la réplcation propre du conteneur si besoin plus de puissance.

 Le depencance OS = From **php:7.4-apache** *(cf DockerFile)*

 Le DockerFile *(Format yaml)* quand il est build il passe en binaire

 Une autre plateforme comme docker hub => Quay.io
 
 *Permet le scan de vilnerabilités sur le DockerFile*

 Un tag permet de choisir une version du *From* adapté à nos besoin

 !!! note
 	 Ne pas utilisé le **latest** en **Prod**

 *Car cela peut monter en version une image qui n'est pas compatible*

Dans le dockerfiles il faut ajouter les dépendance que l'on à besoin 
 
## Les noeuds
Node = machine vituelle/physique possedant docker

Plusieurs node en redonance = Cluster 

## Le Stockage
*IMMUABLE => Qui reste identique, ne change pas.*

On dit qu'un conteneur est immuable, car il na pas de donnée de a conserver dans un conteneur 
Tous le sockage persistant n'est pas stocker dans le conteneur  

## La fin de Docker
Le moteur docker à des probleme de sécurités
- Il lance ses processus en root donc tous les utilisateurs dans le groupe docker peuvent faire une élévation de privilege
- Il exite donc des fork de docker *(podman,cri-o,containerd,...)*

Ce sont les mêmes commandes que docker et il mange les mêmes fichiers yaml 

## Accès à la machine virtuelle
```
Usr : utilisateur
mdp : GbAa#a93pcJx#SRH
@IP : 20.56.173.220
```

## Cloud 
- Saas (Software as a service / Application clé en main)
- Paas (Platform as a service / middleware access)
- Iaas (Infrastructure as a service / Routing and switching)

## Sur la vm 
- Installation de docker engin *https://docs.docker.com/engine/install/debian/*
- Executer docker au lancement de la vm `sudo systemctl enable docker.service`

### Déploiment du container who am i
!!! note "**BONUS**" 
	*On veux le deployer sur le port 1025*

```
docker container run -d -p 80:1025 -e "WHOAMI_PORT_NUMBER=1025"  traefik/whoami
```

```
-d : detach permet de le lancer sans entrer dans le shell interactif
-e : Lui donner une variable d'environnement *(Ici mapper sur le port 1025 en écoute)*
-p : affecter un port hôte au port du conteneur 
traefik/home : image sur la quelle on veut déployer le conteneur 
```

## Le stockage persistant
	- Avec l'image mariabd:latest 
	- Utilisateur de mariadb nommée **Wordpress** avec mdp **DockerRocks**
	
**BONUS** *Faire un show databases*
```
docker container run --name db -it -d -e MARIADB_DATABASE=Wordpress -e MARIADB_USER=Wordpress -e MARIADB_PASSWORD=DockerRocks -e MARIADB_RANDOM_ROOT_PASSWORD=true mariadb:latest
```

L'image mariaDB Demande des variable d'environement obligation on peut voir cela avec 
```
docker logs container_name
```

*Bonus* : 
```
Docker exec -it mysql -u Wordpress -pDockerRocks Wordpress -e  'SHOW DATABASES;
```

Faire un wp lier a la db

```
docker run --name wp --link db -d -e WORDPRESS_DB_HOST=db -e WORDPRESS_DB_USER=Wordpress -e WORDPRESS_DB_PASSWORD=DockerRocks -e WORDPRESS_DB_NAME=Wordpress -p 80:80 wordpress
```

!!! warning 

	**/!\ Link est une pratique déprcié ne pas utiliser**

Test avec les variable du link

```
docker run --name wp --link db -d -e WORDPRESS_DB_HOST=db -e WORDPRESS_DB_USER=$DB_ENV_MARIADB_USER -e WORDPRESS_DB_PASSWORD=$DB_ENV_MARIADB_PASSWORD -e WORDPRESS_DB_NAME=$DB_ENV_MARIADB_DATABASE -p 80:80 wordpress
```
**NE MARCHE PAS**

# Jour 2 08/12

|VM|
|:-------|
| 1 ou plus app |
| V.OS |
|V.HARD|
|Hyperviseur/Os|
|HARD|



## Avantage de la contnerisation
1. Performance
2. Ressources
3. MCO Plus
    - \+ Vite
	- \+ Léger
	- Versionning
4. Prix
5. Gain d'agilité

!!! tip "DEF Orchestrateur de conteneurs"

	L'orchestration des conteneurs permet d'automatiser le déploiement, la gestion, la mise à l'échelle et la mise en réseau des conteneurs

L'orchestrateur de conteneurs gères les ressources des moteurs de contneurisation

### Kubernetes
!!! note "Kubernetes"

	Solution open souce, développer par google et offert à la *linux fondation* en 2014 cela créer à la **cncf** 

C'est une solution portable et modulable [*toutes les bricks peuvent être changer*](https://landscape.cncf.io/)

Kubernestes(et d'autres) possede des surcouche qui s'appele des **Platemeforme de d'orchestation d'entreprise**

Tout cela peuvent être gérer par des Solution multi-cloud 

Permet d'avoir une vu unique de plusieurs environement cloud 

Pour une architecture minimal kubernetes doit posseder 

- Un master
- Deux workers 

Le master permet le pilotage des nodes/workers

Il est composé de :
- ETCD (Base de donnée en clé/valeur pour la gestion)
- Scheduler (Assure les disponibilité de ressouces et gere à repondre au besoin)
- controller manager 
- **API-SERVER** (Interface avec l'utilisateur le **coeur** de kube)

*Toutes ces service sont dans des conteneurs* 

Les workers servent deployer les conteneurs

Ils sont composés de :
- Kublet (Lien avec le moteur de conteneurisation *n'est pas contneurisé)
- Pod (Héberge l'applicatif)
- Kube Proxie

## Vocabulaire
|Terme | Signification
|:-----|:-----|
|Container | Ressource exécutant une application
|Pod | Grouppement de plusieurs conteneurs
|Deployment| Orchestre les *pod* et *replicaSets*|
|Deamonset|S'assure d'avoir un pod sur chaque noeud|
| statefullset | [legacy] S'assure de n'avoir qu'un seul pod            |
| service | Expose votre application sur le réseau du cluster      |
| ingress  | Permet d'exposer un service en externe (#ReverseProxy) |
| persistent volumes   | Défini un accès au stockage                            |
| persistent volumes claims | Demande d'allocation de stockage |
| configMaps| stockage clé-valeur non confidentiel                   |
| secrets| stockage clé-valeur confidentiel                       |

!!! note
	
	Pour faire communiquer deux pods on a besoin d'un **service**

## Installation de kubeclt

[Documentation](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/)


!!! tip "Changer le lien du fichier de conf Kube"
	```bash
	export KUBECONFIG="/mnt/c/Users/v.galves/OneDrive/Cours/Cloud-Native/.kube/config"
	```

Pour lister les espace de travail (espace de nom)

```bash
kubectl get namespace
```
Pour créer un espace de nom 
```bash
kubectl create namespace valentin
```

### Creation d'un conteneur
```bash
kubectl create deployment --image nginx:latest -- port 80 nginx
```

Voir les déployment
```bash
kubectl get deployment
```

On veux qu'il sit accessible depuis internet 
Il faut donc créer un service
```bash 
kubectl create service loadbalancer --tcp 80:80 nginx
```

!!! tip

	Pour supprimé un élément à la place de `create` mettre `delete`

Pour éditer notre fichier déployment 
``` bash
kubectl edit deployment
```
### Exemples de manifests

``` yaml
# nginx-deployment.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```
Manifeste de service plus léger 
``` yaml
        
# nginx-service.yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector: #Permet de ratacher le service au deployment
    app.kubernetes.io/name: MyApp 
  ports:
    - protocol: TCP
      port: 80 #Port extene 
      targetPort: 9376 #Port interne
```

## TP
### 1 - Mon premier conteneur Kubernetes

Dans cet exercice, vous allez déployer un *conteneur* dans un *POD* unique propulsé par un simple *déploiement*.

- [ ] Création d'un *deploiement* nommé whoami
  - [ ] Utilisant l'image **containous/whoami**
  - [ ] Utilisant le port interne **80/TCP**
- [ ] Création d'un service nommé **whoami**
  - [ ] Ecoutant sur le port **80**
  - [ ] Lié au deploiement **whoami**
  - [ ] Type de service: **NodePort**
  - [ ] Récupérer le port utilisé sur la machine hôte
- [ ] Accéder au service whoami depuis votre poste client

!!! tips 
	Pour sortir un yaml avec les modification a chaud ``--dry-run=whoami -o yaml``

Regarder les log des pods 
``` bash
kubectl logs -f deployments/whoami
```
Lancer un shell dans un pod **/!\ Mauvaise pratique**
```bash
kubctl exec {pod-name} -- sh
```

Pour *"push"* un ficher yaml
```bash 
kubectl apply -f
```
