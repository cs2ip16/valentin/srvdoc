# Administration Réseaux


| Interface | LAN SEGMENT | @REZO Attribuée |
| ----- | ----- | ----- |
| ens160 | CCILAN | 10.56.126.0 |
| ens224 | LINUX-LAN1 | 192.168.111.0 |
| ens256 | LINUX-LAN2 | 192.168.112.0 |

## Network Manager
!!! note "DEF"

    NetworkManager est un service réseau système qui gère vos périphériques et connexions réseau et tente de maintenir la connectivité réseau active lorsqu'elle est disponible.

Commande de base *nmcli/nmtui* 
- cli pour linge de commande 
- tui pour une semi-interface graphique
  
Pour la doc : *man nmcli*


On veut un topo sur : 
- les commandes be base pour visualiser la configuration, les devices, les connexions 
    - nmcli
    - nmcli device show
    - nmcli connection show

- l'interface lo apparaît comme non gérée, on veut donc savoir comment de dire à Network Manager que telle interface est gérée par lui ou pas. 
    - 

- quelle est la procédure de déclaration d'une nouvelle "connexion" ? [LA DOC](https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/8/html/configuring_and_managing_networking/configuring-networkmanager-to-ignore-certain-devices_configuring-and-managing-networking)
    - Crée un ficher */etc/NetworkManager/conf.d/99-unmanaged.conf*
```conf
[keyfile]
unmanaged-devices=mac:00:0c:29:fb:fe:66,mac:00:0c:29:fb:fe:70
```

- Comment établir une configuration d'une connexion avec adressage statique - Comment établir une configuration d'une connexion avec adressage DHCP
    - nmcli connection add ipv4.method manual ipv4.addresses 10.56.126.221/24 ipv4.gateway 10.56.126.254 ipv4.dns 1.1.1.1 type ethernet con-name lancci 
    - nmcli connection up lancci


## Networkd

!!! note "DEF"

    **systemd-networkd** est un daemon système qui gère les configurations réseau. Il détecte et configure les périphériques réseau à mesure qu'ils apparaissent ; il peut également créer des périphériques réseau virtuels. Ce service peut être particulièrement utile pour mettre en place des configurations réseau complexes pour un conteneur géré par systemd-nspawn ou pour des machines virtuelles. Il fonctionne également très bien sur des connexions simples.



On veut un topo sur :

- les commandes be base pour visualiser la configuration, les devices, les connexions
    - networkctl
  

- l'interface lo apparaît comme non gérée, on veut donc savoir comment de dire à Network Manager que telle interface est gérée par lui ou pas.
- quelle est la procédure de déclaration d'une nouvelle "connexion" ?
- Comment établir une configuration d'une connexion avec adressage statique
- Comment établir une configuration d'une connexion avec adressage DHCP

Pour gere une configuration créer un fichier .network dans */etc/systemd/network/*
```conf
[Match]
MACAdress=00:0c:29:fb:fe:66
[Network]
Address=192.168.111.254/24
```

!!! tip

    Avoir des info noyau avec *uname*

![SchémaReseau](Images/Shemareseau.png)

## Gestion reseau 
#### Routage 

On change une valeur du noyau pour activer le routage

```bash title="Activation du routage sur la machine 1"
echo "net.ipv4.ip_forward = 1" > /etc/sysctl.d/01-routage.conf
# Ou de maniere non permanante 
sysctl -w net.ipv4.ip_forward=1
```

!!! tip "Verifier le routage"
    
    ```sysctl net.ipv4.ip_forward```

Si il est sur 1 c'est que c'est bon

#### Regles de parfeu

On vas utilier **firewalld** pour la getion du parefeu linux

Par defaut le parametre n'est pas permantant il faut rajouter l'attribut ```--permanant```

On veux ajouter tous les @IP 192.168.0.0/16 en ip de confiance 

```
firewall-cmd --zone=trusted --add-source=192.168.0.0/16
```
Une fois que notre configuration nous convient on peut refaire la ligne de commande avec notre attribut 

#### Route statique 

On fait les routes pour accèder aux reseaux des voisins 

```bash title="Exemple vers le reseau de fmicaux"
#fmicaux
nmcli connection modify lancci +ipv4.routes "192.168.101.0/24 10.56.126.220"
nmcli connection modify lancci +ipv4.routes "192.168.102.0/24 10.56.126.220"
#Anto
nmcli connection modify lancci +ipv4.routes "192.168.121.0/24 10.56.126.222"
nmcli connection modify lancci +ipv4.routes "192.168.122.0/24 10.56.126.222"
#Léo
nmcli connection modify lancci +ipv4.routes "192.168.131.0/24 10.56.126.223"
nmcli connection modify lancci +ipv4.routes "192.168.132.0/24 10.56.126.223"
#Jordix
nmcli connection modify lancci +ipv4.routes "192.168.141.0/24 10.56.126.224"
nmcli connection modify lancci +ipv4.routes "192.168.142.0/24 10.56.126.224"
#Arturio
nmcli connection modify lancci +ipv4.routes "192.168.151.0/24 10.56.126.225"
nmcli connection modify lancci +ipv4.routes "192.168.152.0/24 10.56.126.225"

```

Avec Systemd-networkd

De clone 2 Vers tous
Dans le fichier */etc/systemd/network/lan2.network*
On rajoute

```conf
[Route]
Destination=0.0.0.0/0
Gateway=192.168.112.254
```
Si on veut faire plusieurs route on met plusieurs blocks **[Route]**

```conf title="Exemple"
#fmicaux
[Route]
Destination=192.168.101.0/24
Gateway=10.56.126.220
[Route]
Destination=192.168.102.0/24
Gateway=10.56.126.220
#Anto
[Route]
Destination=192.168.121.0/24
Gateway=10.56.126.222
[Route]
Destination=192.168.122.0/24
Gateway=10.56.126.222
#Léo
[Route]
Destination=192.168.131.0/24
Gateway=10.56.126.223
[Route]
Destination=192.168.132.0/24
Gateway=10.56.126.223
#Jordix
[Route]
Destination=192.168.141.0/24
Gateway=10.56.126.224
[Route]
Destination=192.168.142.0/24
Gateway=10.56.126.224
#Arturio
[Route]
Destination=192.168.151.0/24
Gateway=10.56.126.225
[Route]
Destination=192.168.152.0/24
Gateway=10.56.126.225
```

```conf title="Configuration dns"
[Resolve]
DNS=10.56.126.221
Domains=local.
```

#### NAT

Pour faire du NAT avec firewalld il suffit de déplacer l'interface vers la zone external
```bash title="Activation du NAT"
firewall-cmd --zone=public --remove-interface=ens160 --permanent
firewall-cmd --zone=external --add-interface=ens160 --permanent
```

## DNS 
#### Ressources :
[Pad](https://pad.actilis.net/p/cs2i-admav) \
[Cours](https://cl.actilis.net/index.php/s/frydpMr6wT8CQGN)

Commande de base pour résoudre des noms dns ```getent```

!!! info

    La commande **getent** affiche les entrées des bases de données prises en charge par les bibliothèques du Name Service Switch (NSS), qui sont configurées dans /etc/nsswitch.conf. Si un ou plusieurs arguments clé sont fournis, alors seules les entrées correspondantes aux clés fournies seront affichées. Sinon, si aucune clé n'est fournie, toutes les entrées seront affichées (sauf si la base de données ne permet pas l'énumération).

!!! note 

    Des commandes de gestion de nom de domaine avec le paquet *bind*
    nslookup, host, et une autre


Les types d'enregistrements dns :

  - SOA (Start Of Authority)
  - A
  - C
  - NS 
  - MX
  - SRV
  - LOC (Indique la location de la machine) 
  - PTR (Permet de faire des )

!!! info "SPF"

    Le protocole Simple Mail Transfer Protocol (SMTP) utilisé pour le transfert du courrier électronique sur Internet ne prévoit pas de mécanisme de vérification de l'expéditeur, c'est-à-dire qu'il est facile d'envoyer un courrier avec une adresse d'expéditeur factice, voire usurpée. **SPF** vise à réduire les possibilités d'usurpation en publiant, dans le DNS, un enregistrement (de type TXT)3 indiquant quelles adresses IP sont autorisées ou interdites à envoyer du courrier pour le domaine considéré.


!!! info "DKIM"

    DKIM (DomainKeys Identified Mail) est une norme d'authentification fiable du nom de domaine de l'expéditeur d'un courrier électronique. Elle constitue une protection efficace contre le spam et l'hameçonnage.
    En effet, **DKIM** fonctionne par signature cryptographique du corps du message ou d'une partie de celui-ci et d'une partie de ses en-têtes. Une signature DKIM vérifie donc l'authenticité du domaine expéditeur et garantit l'intégrité du message. DKIM intervient au niveau de la couche application du modèle OSI, ainsi il constitue une double protection pour des protocoles de messagerie électronique tels que SMTP, IMAP et POP en plus de l'utilisation de ces protocoles en mode sécurisé (POPS, IMAPS).


### Création de serveur DNS

 Le but est de créer un serveur master qui sera salve de notre gentil voisin


!!! note "Voir ce qu'un paquet à installer"

    ```rpm -ql bind```
    *Grep* pour faire du trie
    **systemd** et **/etc/** sont des filtre interésants
    

```C title="/etc/named.conf"
// On a rajouter/modifier les lignes suivantes
acl mes-clients {
        10.56.126.0/24;
};
options {
        listen-on port 53 { 127.0.0.1; 10.56.126.221 }; // ajout de notre @ip LAN
        allow-query     { localhost; mes-clients; }; // ajout de notre acl mes client qui accepte les @ip de notre lan
        //Ajout des droit pour les dns slave
        allow-transfer {
            mes-clients;
        };
};
include "/etc/named/mesdomaines.conf"; // inclure le fichier de configuration de domaine
```

Et nous fesons notre ficher de configuration de domaine 
```conf title="/etc/named/mesdomaines.conf"
zone "dom1.local." {
        type master;  type du serveur { master || slave }
        file "dynamic/dom1.local.zone"; fichier de zone prensent         
};
```

```conf title="/var/named/dynamic/dom1.local.zone"
$TTL 3600 ;valeur par defaut du ttl cela permet de ne pas le presiser a chaque enresitrement 
@       IN      SOA     dns1 admin (
                        2023021401 ; Serial
                        3600       ; Refresh
                        1800       ; Retry
                        604800     ; Expire
                        86400 )    ; Minimum TTL
@       IN      NS      ns1
ns1     a       10.56.126.221 ;
```

```conf title="Ajout de 4 zones en type secondaire"
zone "dom0.local." { type slave ; masters { 10.56.126.220; }; };
zone "dom2.local." { type slave ; masters { 10.56.126.222; }; };
zone "dom3.local." { type slave ; masters { 10.56.126.223; }; };
zone "dom4.local." { type slave ; masters { 10.56.126.224; }; };
zone "dom5.local." { type slave ; masters { 10.56.126.225; }; };
```
