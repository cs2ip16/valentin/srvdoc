# Sécurité linux

## Ressources

[lien du pad](https://pad.actilis.net/p/cs2i-admav)

[Lien du support de cour](https://cl.actilis.net/index.php/s/frydpMr6wT8CQGN)

## Permission

On utilise la commande **chsh**

Dans les droit il y a la lettre "s"
Le **S** permet d'executer la commande avec l'identité du propriétaire du fichier

Pour auditer les fichier/Dossier avec ce paramettre

```bash

sudo find / -perm /g=s -o -perm /u=s
```

!!! warning

    Cela peut être une faille de sécurité

Le **t** correspond à Sticky-bit *collant au proprio* :

    Seul le propriétaire du fichier à le droit de le supprimé

**Pour changer les permissions d'un fichier il faut être le propriétaire**
*Sauf root qui n'a pas de restriction*

Pour le chmod on peux ajouter information de permission numéraire
*à inserer avant les permission utilisateur*

- 1 Pour le sticky-bit
- 2 pour le sgid *(Prendre l'identité du groupe à l'execution)*
- 4 pour le suid *(Prendre l'identité du propriétaire à l'execution)*

!!! tips

    Manuel des privilege
    ```
    man capabilities
    ```
    il nous faut le packet man-pages

Ou sur [internet](https://man7.org/linux/man-pages/man7/capabilities.7.html)

Les capapilities permettent de :

- la gestion des privileges très détaillé
- Donner des droits sur certain processus  à un utilisateur sans donner les droits *sudo*

Certains parametres

- CAP_DAC_OVERRIDE : Celui de root **(Aucune restriction)**
- CAP_CHOWN : Permet de faire la commande chown sur tous les repertoire

## TP - avoir l'autorisation d'écouter sur un port privilegier

Faire ecouter un utilisateur courrant sur un port privilegier *(>1024)*

*ici on vas utiliser l'utilisateur test (avec le mdp Hd$KsK#H8fdDJi&i)*

!!! Note "Prerequis"

    Nous avons besoin du paquet *nc* pour écouter sur un port 

```
dnf install -y nc
```

Ensuite

On lance un serveur avec l'utilisateur root

```bash
nc localhost 90
```

Et avec un **autre terminal** avec un utilisateur courrant sans privilege on ecoute sur se port

```
sudo nc -l 90
```

```bash title="Avec l'utilisateur test"
$nc -l 90
Ncat: bind to :::90: Permission denied. QUITTING.
```

On donne les droits à utiliser les programes sur les ports privilegiés à l'utilisateur **Test*

!!! note "Packet nessesaire"

    Debian - libcap2-nin
    Redhat - libcap-ng-utils

- execcap : pour limiter privilèges hérités par un programme
- getpcaps : affiche les privilèges d’un processus
- capsh : un wrapper bash pour « jouer avec les privilèges »
- setcap : modifier les privilèges d'un fichier
- getcap : lister les privilèges d'un fichier

```bash
#On copie le ficher executable vers tmp    
install /bin/nc /tmp/capnc -o test -g test -m 700
#On set le droit sur le ficher precedement copié 
sudo setcap CAP_NET_BIND_SERVICE=pe /tmp/capnc 
```

!!! note

    - e = effectif (Il vas l'utilisé)
    - p = permissif (il a le droit de l'utilisé)

Cela ne fonctionne que en local car le pare-feu bloque l'acces par default

```bash title="Ouverture du port 90"
firewall-cmd --add-port=80/tcp
```

!!! info

    ceci est temporairement


## SELinux

!!! info "DEF SELinux"

    SELinux définit les contrôles d'accès pour les applications, processus et fichiers d'un système. Il utilise des politiques de sécurité, c'est-à-dire des ensembles de règles qui lui indiquent ce à quoi un utilisateur peut accéder ou non, pour mettre en application les autorisations d'accès définies par une politique.

SELinux possede une strategie par default de bloquer **tous** ce qui nest pas autorisé

Avoir le status selinux

```bash
[root@alma9-1-vg ~]# sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   enforcing
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33
[root@alma9-1-vg ~]# 
```

> Voir les reglages SELinux se fait dans le fichier `/etc/selinux`

Pour changer son status

```bash
setenforce permissive
```

Cette commande n'est pas perssitante pour le rendre perssitant il faut le faire dans le fichier `/etc/selinux`

SeLinux donne des droits a des familles de **processus** sur des familles **fichiers** grace a des **tags**

- Processus = domaine
- Fichier = type

Pour voir les tags SELinux
Si un fichier possede un label de sécurité il possede un point a la fin des droits

[root@alma9-1-vg ~]# ls -lZ
total 24
-rw-------. 1 root root system_u:object_r:**admin_home_t**:s0     1313  1 déc.  10:14 anaconda-ks.cfg
drwxr-xr-x. 2 root root unconfined_u:object_r:**admin_home_t**:s0 4096  1 déc.  16:44 new-iso
drwxr-xr-x. 6 root root unconfined_u:object_r:**admin_home_t**:s0 4096  2 déc.  16:31 rpmbuild
-rw-r--r--. 1 root root unconfined_u:object_r:**admin_home_t**:s0 1054  9 déc.  20:04 RPM-GPG-KEY
-rw-r--r--. 1 root root unconfined_u:object_r:**admin_home_t**:s0 1753  4 déc.  15:46 vgalveskey
-rw-r--r--. 1 root root unconfined_u:object_r:**admin_home_t**:s0 3516  4 déc.  15:13 vgalveskeysha256

Pour reinitialiser les labels se linux par default il faut mettre un fichier nomée **".autorelabel"**
Cela vas chercher les default templates present dans `/etc/selinux/tageted`

Avoir le tag d'un processus

```bash
ps -Zax
```

## TP SELINUX

### Consigne

Illustration de SELinux

1. Arrêtez le service auditd (avec la commande "service auditd stop" si systemctl ne veut pas l'arrêter) Supprimez le fichier /var/log/audit/auditd.log Démarrez le service auditd.

Si selinux est désactivé, réactivez le, en mode permissif (/etc/selinux/config)

2. Reconstruisez les contextes de sécurité : "restorecon -R /" ==> On peut forcer cette reconstruction au prochain reboot : "touch /.autorelabel"

3. Installez un serveur FTP (vsftpd)

4. Intéressez-vous au répertoire /var/ftp et /var/ftp/pub Intéressez-vous aux processus "vsftpd" Créez un répertoire "/home/ftp", et "/home/ftp/pub" Faites en sorte que /home/ftp soit le répertoire de base du FTP anonymous (anon_root=...)

Passez selinux en mode "enforcing" Constatez que vous ne pouvez plus accéder à votre serveur FTP correctement. Passez le de nouveau en mode "permissive" : cela remarche.

Analysez (regardez) le fichier /var/log/audit/auditd.log

5. Utilisez audit2why et pourquoi pas audit2allow pour "produire" une règle (un module selinux que vous intégrez à votre "policy") Constatez que maintenant, c'est OK pour FTP...

Mais ce n'est pas la bonne approche, il y a déjà des règles concernant FTP.

6. Supprimez votre module si vous en avez créé un,

À la place, restaurez le bon contexte de sécurité pour /home/ftp ==> utilisez "chcon", et inspirez-vous de celui de /var/ftp

7. Forcez une reconstruction des contextes de sécurité au prochain reboot, Passez selinux en mode "enforcing" (dans le fichier de config) et rebootez ==> votre serveur FTP "/home/ftp" ne marche plus !

Afin que désormais les contextes de sécurité soient automatiquement restaurés, éditez le template qui est utilisé par la reconstruction automatique afin que /home/ftp dispose du bon contexte de sécurité (idem à celui de /var/ftp)

==> /etc/selinux/targeted/contexts...

==> plutôt que de rebooter suite à un "touch /.autorelabel", forcez une reconstruction par "restorecon -R /"

### Execution

1. 

```bash
sudo systemctl stop auditd
Failed to stop auditd.service:
#La commande systemctl ne veux pas
sudo service auditd stop
Stopping logging: 
#Service veux bien 
sudo rm -r /var/log/audit/audit.log
systemctl start auditd.service
```

2. 

```bash linenums="1"
[vgalves@alma9-1-vg ~]$ sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   permissive
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33
```

3. 
 
[vgalves@alma9-1-vg ~]$ sudo restorecon -R /
restorecon: Could not set context for /run/docker/netns/6cbd57820ee6:  Operation not permitted

4. 

```bash
sudo dnf -y install vsftpd
ls -lZ /var/ | grep --color ftp
```

drwxr-xr-x.  3 root root system_u:object_r:**public_content_t**:s0 4096 26 janv. 16:19 ftp

Ici on peux observer le label **public_content_t**

drwxr-xr-x. 2 root root system_u:object_r:**public_content_t**:s0 4096 11 nov.  16:15 pub

Le repertoire pub possede le même tag

```bash
system_u:system_r:ftpd_t:s0-s0:c0.c1023 1883 ?   Ss     0:00 /usr/sbin/vsftpd /etc/vsftpd/vsftpd.conf
```

Le tag du processus est **ftpd_t**

essaye de connexion
<ftp://10.56.126.48>

!!! tips "Il faut ouvrir les ports en question"
    
    ```bash
    sudo firewall-cmd --add-service=ftp
    ```

> annoymous n'est pas accepter il faut commanter a ligne `anonymous_enable=NO`

On veux changer le fichier racine de la connection ftp en anonyme

sudo nano /etc/vsftpd/vsftpd.conf

Ajout de la ligne
> anon_root=/home/ftp/

Tag du repertoire /home/ftp
**user_home_dir_t**

Étrangement cela fonctionne il doit avoir une regles selinux qui autorise les service ftp à utiliser le respertoir */home/ftp* par default

!!! info Il nous faut d'autres paquets utilitaire

    setool-console

sudo sesearch -A -s tftpd_t | grep " tftpd_t"

```bash

allow tftpd_t user_home_dir_t:dir { add_name remove_name write }; [ tftp_home_dir ]:True
allow tftpd_t user_home_dir_t:dir { add_name remove_name write }; [ tftp_home_dir ]:True
allow tftpd_t user_home_dir_t:dir { add_name remove_name write }; [ tftp_home_dir ]:True
allow tftpd_t user_home_dir_t:dir { add_name remove_name write }; [ tftp_home_dir ]:True
allow tftpd_t user_home_dir_t:dir { add_name remove_name write }; [ tftp_home_dir ]:True
allow tftpd_t user_home_type:dir { add_name create link remove_name rename reparent rmdir setattr unlink watch watch_reads write }; [ tftp_home_dir ]:True
allow tftpd_t user_home_type:dir { add_name remove_name write }; [ tftp_home_dir ]:True
allow tftpd_t user_home_type:dir { add_name remove_name write }; [ tftp_home_dir ]:True
allow tftpd_t user_home_type:dir { add_name remove_name write }; [ tftp_home_dir ]:True
allow tftpd_t user_home_type:dir { add_name remove_name write }; [ tftp_home_dir ]:True
allow tftpd_t user_home_type:dir { add_name remove_name write }; [ tftp_home_dir ]:True

```

On peut voir que le tag **user_home_dit_t** est autorisé

On vas switch sur le paquet apache
A suivre

### TP 3

On veux faire ecouter notre seveur apache sur le port 8888

Dans le fichier `/etc/httpd/conf/httpd.conf`

On change la directive `Listen 8000` > `Listen 8888`

Cela met en écoute le service httpd sur le port 8888

Quand on lance le service httpd

```bash

(13)Permission denied: AH00072: make_sock: could not bind to address 0.0.0.0:8888
Failed to start The Apache HTTP Server.

````

On vas voir les logs de selinux

```bash
cat /var/log/audit/audit.log | grep httpd

type=AVC msg=audit(1674814839.101:966): avc:  denied  { name_bind } for  pid=4792 comm="httpd" src=8888 scontext=system_u:system_r:httpd_t:s0 tcontext=system_u:object_r:unreserved_port_t:s0 tclass=tcp_socket permissive=0
```

Il n'a pas le droit de diffuser sur le port 8888

Pour lister les port en lien avec les tags

```
semanage port -l | grep http

http_port_t                    tcp      80, 81, 443, 488, 8008, 8009, 8443, 9000
```

On voit bien quil n'y a pas le **8888** \
On le rajoute

```
sudo semanage port --add 8888 -t http_port_t -p tcp
```

It's WORKS

!!! info "Def CGI"

    La common gateway interface (CGI) est une interface de serveurs Web qui permet un échange de données normalisé entre des applications et des serveurs externes. Elle fait partie des plus anciennes technologies d'interface de l'Internet et est encore fréquemment utilisée aujourd'hui.

Ensuit dans le ficher */var/www/cgi-bin/env.cgi*

```bash
#!/bin/bash

echo -e "Content-Type: text/plain\n\n"

echo "la commande est : $QUERY_STRING"
CMD=$(echo $QUERY_STRING | sed 's,%20, ,g')
$CMD

#set
```

On peux envoyer des commandes depuis le navigateur

```bash
curl http://127.0.0.1:8888/cgi-bin/env.cgi?cat%20/etc/passwd

la commande est : cat%20/etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
tss:x:59:59:Account used for TPM access:/dev/null:/sbin/nologin
sssd:x:998:995:User for sssd:/:/sbin/nologin
chrony:x:997:994:chrony system user:/var/lib/chrony:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/usr/share/empty.sshd:/sbin/nologin
systemd-oom:x:992:992:systemd Userspace OOM Killer:/:/usr/sbin/nologin
agueheneuc:x:1000:1000:Antoine:/home/agueheneuc:/bin/bash
vgalves:x:1001:1001::/home/vgalves:/bin/bash
hacluster:x:189:189:cluster user:/home/hacluster:/sbin/nologin
rpc:x:32:32:Rpcbind Daemon:/var/lib/rpcbind:/sbin/nologin
rpcuser:x:29:29:RPC Service User:/var/lib/nfs:/sbin/nologin
test:x:1002:1002::/home/test:/bin/bash
test1:x:1003:1003::/home/test1:/bin/bash
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
```

!!! Warning

    Il est donc conseiller de ne pas utiliser CGI 

    - Soit le deconfigurer dans apache 
    
    - Soit l'interdire avec SELinux

### Interdire des options selinux avec les booleen

```
getsebool -a | grep httpd
httpd_anon_write --> off
httpd_builtin_scripting --> on
httpd_can_check_spam --> off
httpd_can_connect_ftp --> off
httpd_can_connect_ldap --> off
httpd_can_connect_mythtv --> off
httpd_can_connect_zabbix --> off
httpd_can_manage_courier_spool --> off
httpd_can_network_connect --> off
httpd_can_network_connect_cobbler --> off
httpd_can_network_connect_db --> off
httpd_can_network_memcache --> off
httpd_can_network_relay --> off
httpd_can_sendmail --> off
httpd_dbus_avahi --> off
httpd_dbus_sssd --> off
httpd_dontaudit_search_dirs --> off
httpd_enable_cgi --> on
httpd_enable_ftp_server --> off
httpd_enable_homedirs --> off
httpd_execmem --> off
httpd_graceful_shutdown --> off
httpd_manage_ipa --> off
httpd_mod_auth_ntlm_winbind --> off
httpd_mod_auth_pam --> off
httpd_read_user_content --> off
httpd_run_ipa --> off
httpd_run_preupgrade --> off
httpd_run_stickshift --> off
httpd_serve_cobbler_files --> off
httpd_setrlimit --> off
httpd_ssi_exec --> off
httpd_sys_script_anon_write --> off
httpd_tmp_exec --> off
httpd_tty_comm --> off
httpd_unified --> off
httpd_use_cifs --> off
httpd_use_fusefs --> off
httpd_use_gpg --> off
httpd_use_nfs --> off
httpd_use_opencryptoki --> off
httpd_use_openstack --> off
httpd_use_sasl --> off
httpd_verify_dns --> off



setsebool httpd_enable_cgi off
```

   - -P permet de rendre les regles persistantes

[Liste des booleans](https://wiki.centos.org/TipsAndTricks/SelinuxBooleans)

## Sudo et Sudoreplay

!!! info "DEF Sudo"

    sudo (abréviation de "substitute user do", "super user do" [2] ou "switch user do " [3], en français : « se substituer à l'utilisateur pour faire », « faire en tant que super-utilisateur » ou « changer d'utilisateur pour faire ») est une commande informatique utilisée principalement dans les systèmes d'exploitation de type Unix.

Sudo permet de faire de la gestion de droit pour les utilisateur/groupes 

Cela se definie dans le fichier **/etc/sudoers**

!!! info Droping directory

    Si on met un fichier dans **/etc/sudoers.d/** il sera interpreté aussi

Mise en place du logging sur les sessions sudo

Creation d'un repertoire pour stocker les logs

```bash
mkdir -p /var/log/sudo-io
```

Ajout des lignes suivantes dans `/etc/sudoers`


```bash linenums="1"
#Temps attente du mdp en mins
Defaults passwd_timeout = 1
#Temps pour le quel il ne redemandera pas de mot de passe pour les commande sudo
Defaults timestamp_timeout = 2
#Set log_output to default > by default is /var/log/sudo-io
Defaults log_output
Defaults!/usr/bin/sudoreplay !log_output
Defaults!/sbin/reboot !log_output
```

le but est que les utilisateurs dans le groups *users* puissent effectuer les commandes :
 - swapon
 - swapoff que avec l'argument "/dev/sda3"

```bash
#ALIAS
User_Alias USERS = test
Cmnd_Alias SWAP = /sbin/swapon, /sbin/swapoff /dev/nvme0n1p3

USERS ALL = NOPASSWD: SWAP
```

```bash
sudo swapoff /dev/nvme0n1p2
sudo swapon
sudo swapon -a
sudo swapoff -a
[sudo] Mot de passe de test : 
```


!!! info "SUDOREPLAY"

    sudoreplay rejout ou liste les logs créés par sudo. Il peut jouer les sessions en temps réel, ou en ajustant la vitesse. 

## identification et analyser les logs 

- Logs de connexion ssh:

```bash
journalctl -u sshd   
```

Protéger le service sshd¶
Identifier et analyser les logs¶
Les logs des (tentatives de) connexion

Se protéger des attaques brute-force

Analyse de logs avec sshguard

Analyse de logs avec fail2ban
Utiliser un port-knocker¶
Solution de port-knocker : mise en place de knockd avec règles FW...

nécessite de trouver (compiler ?) le package

Installation du serveur knockd

nécessite des bases sur Netfilter

Détecter les scan de ports
Scan de port et contre-mesure : scanlogd

TP du dimanche... bloquer les scan de ports
Ecriture d'une règle dans sshguard (si possible) fail2ban (c'est possible) pour - 1/ détecter les scan de port - 2/ logguer chaque scan de port - 3/ demander à fail2ban de compter les scan de ports loggués et de bloquer les IPs qui nous ont scanné

Un audit de sécurioté local avec Lynis



### Sécuriser le SSH
Dans la conf de ssh on peut authoriser a se connecter en root **Mauvaise idée** 
Avec la clé rsa peux être une bonne idée 

#### FAIL2BAN
On veut ajouter le paquet fail2ban 
Sur pkgs.org on a vu qu'il était sur le repo epel-release
```bash
sudo dnf install epel-release
sudo dnf install fail2ban-systemd
```
!!! note "Pour voir l'etat de fail2ban"

    ```
    fail2ban-client status
    ```

**/!\ NE PAS MODIFIER LE FICHER /etc/fail2ban/jail.conf**
Il faut ajouter un fichier dans **Jail.conf**

On crée donc un fichier 22-ssh.conf
Et on veux que 5 echecs en 5 mins engendrent un bannissement de 30 mins

```conf
[sshd]
enabled=true

maxretry = 5
findtime = 300
bantime = 1800
```

Pour voir les @ip bannie 
```bash
fail2ban-client get sshd banned
```
Pour deban des @IP
```bash
fail2ban-client set sshd unban <@ip>
```

#### SSHGUARD 
Fichier de conf */etc/sshguard* Classique

!!! info "DEF"

    sshguard protège les hôtes des attaques par force brute contre SSH et d'autres services. Il regroupe les journaux système et bloque les récidivistes à l'aide de l'un des nombreux backends de pare-feu, notamment iptables, ipfw et pf. Attaques par force brute sans SSHGuard SSHGuard bloque les attaques par force brute.


#### Knockd

!!! info "DEF"

    Knockd est un service de *Port Knocking SSH* qui permet de d’ouvrir et de fermer les ports d’une machine de façon dynamique.
    Cela permet ouvrir un port (ici SSH) sur le serveur seulement quand celui-ci aura reçu une séquence bien précise de signaux sur des ports définis à l’avance

Il nous faut le paquet knockd
```bash
dnf provides "*/knockd"
knock-server-0.8-2.el9.x86_64 : A port-knocking server/client
Dépôt               : epel
Correspondances trouvées dans  :
Nom de fichier : /etc/sysconfig/knockd
Nom de fichier : /usr/sbin/knockd
#installation du paquet 

dnf install -y knock-server
```
On regarde le fichier de conf */etc/knockd.conf*
```conf
[options]
        UseSyslog

[opencloseSSH]
        sequence      = 2222:udp,3333:tcp,4444:udp
        seq_timeout   = 15
        tcpflags      = syn,ack
        start_command = /sbin/iptables -A INPUT -s %IP% -p tcp --dport ssh -j ACCEPT
        cmd_timeout   = 10
        stop_command  = /sbin/iptables -D INPUT -s %IP% -p tcp --dport ssh -j ACCEPT
```

De base knockd ecoute sur eth0 et on a pas eth0 *ici c'est enp3s0*
C'est une balise dans le fichier de conf *interface = *

Mais nous on veux que cela se fasse avec firewalld

``` name="Pour desactiver le service SSH sur le port 22"
firewall-cmd --zone=public --remove-service=ssh
```


```conf
[options]
        UseSyslog
        interface     = enp3s0 #Pour changer l'interface du knockd
[opencloseSSH]
        sequence      = 2567:udp,5683:tcp,3164:udp
        seq_timeout   = 15
        tcpflags      = syn
        start_command = /sbin/firewall-cmd --add-rich-rule='rule family=ipv4 source address=%IP%/32 service name=ssh m accept'
        cmd_timeout   = 10
        stop_command  = /sbin/firewall-cmd --add-rich-rule='rule family=ipv4 source address=%IP%/32 service name=ssh m deny'
```


 
```bash name="Depuis un autre poste linux"
knock -v 10.56.126.62 2567:udp 5683:tcp 3164:udp && sleep 5 && ssh vgalves@10.56.126.62
````