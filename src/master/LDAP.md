# Gestion d'annuaire

[Pad](https://pad.actilis.net/p/cs2i-admav) \
[Cours](https://cl.actilis.net/index.php/s/frydpMr6wT8CQGN?dir=undefined&openfile=8432)


## Installation de OpenLDAP

!!! info "DEF"

    LDAP a été développé pour proposer aux fournisseurs de services d’annuaires un protocole d’application et d’accès. Le protocole LDAP permet de rechercher, de modifier ou d’authentifier d’importants volumes de données, d’informations et d’éléments dans des services d’annuaires distribués, mais aussi de gérer la communication avec les bases de données desdits annuaires.


Installation du paquet :
> openldap-servers 

### Schema

Les fichiers de stucture de openLDAP se trouve dans ```/etc/openldap/schema```

On configure un objets avec plusuieurs balise :
- NAME - son nom
- SUP - Surchage de (top = rien)
- MUST - Proriété(s) Obligatoire
- MAY - Propriété(s) Facultative
- ABSTRACT - Classe abstraite (Très peu utilisée)
  
La balise **SYNTAX** correspond au chaine de caratere que l'on peut entrer dans l'objet :
- 1.3.6.1.4.1.1466.115.121.1.15 - Directory String syntax
- 1.3.6.1.4.1.1466.115.121.1.27 - Interger syntax
- [Liste de toutes les syntaxes](https://www.alvestrand.no/objectid/1.3.6.1.4.1.1466.115.121.1.html)

!!! note 

    Il est **FORTEMEMENT** conseillé d'utiliser les schemas déjà present que crée les seins 

### Config

Les fichiers de configuration de openLDAP se trouve dans ```cd /etc/openldap/slapd.d/```

Il ne faut pas modifier les fichiers de configuration il faut le faire en ligne de commande 

Pour cela il nous faut le paquet :
> ldap-client

Il exit 3 protocole de gestion d'annuaire :

- ldap - Gestion sur le port 389
- ldaps - Gestion sur le port 636 en echange securisé 
- ldapi - Gestion via les socket unix */var/run/ldapi*(foctionne que en interne)

Pour tenter un authentification aupres de l'annuaire on a la commande ```ldapwhoami``` :

- H : désigne le serveur grâce à une URL indiquant protocol, host, et port
- Y EXTERNAL : mécanisme d'authentification spécifique basé sur l'OS Ne fonctionne que localement (-H ldapi:///) potentiellement pour tout utilisateur Unix.
- D « DN » : pour spécifier le DN de connexion (le login...)
- w mot-de-passe : pour donner le mot de passe sur la ligne de commande
- W pour demander une saisie interactive, -y pour le prendre dans un fichier.

Pour simplifier les retrours on peut utilisé **-L** ou **-LL** ou -**LLL**

```bash title="liste la config"
ldapsearch -Y EXTERNAL -H ldapi:/// -Q -b cn=config
```

#### Les Filtre

!!! note "By fmicaux"

    - = (égal)
    - \* : joker
    - <= (plus petit ou égal)
    - \>= (plus grand ou égal)
    - ~= (approximation)

    Pas d'opérateur < ou > (stricte), mais possibilités de combinaisons avec :

    - ( ) les parenthèses pour délimiter des expressions
    - & (et, intersection)
    - | (ou, union)
    - ! (non)

    Exemple : pour effectuer un test d'infériorité stricte ( A < B) :

    - **(&(A<=B)(!(A=B)))** : A est inférieur ou égal à B, ET A n'est pas égal à B

#### Modification d'une entrer de l'annuaire

Pour se connecter à distance il faut affecter un mot de passe a *root*
```bash
olcAttributeTypes: ( OLcfgDbAt:0.9 NAME 'olcRootPW' EQUALITY octetStringMatch
 SE' ) DESC 'OpenLDAP Root DSE object' SUP top STRUCTURAL MAY cn )
 s $ olcRestrict $ olcReverseLookup $ olcRootDSE $ olcSaslAuxprops $ olcSaslAu
 olcRootDN $ olcRootPW $ olcSchemaDN $ olcSecurity $ olcSizeLimit $ olcSyncUse
olcRootDN: cn=Manager,dc=my-domain,dc=com
```
On peut donc voir que le pour mettre un mot de passe a root il faut modifier l'attribut **olcRootPW**

On peut faire avec un fichier 
```conf
dn: olcDatabase={2}mdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: discret 

```
```bash title="Envoie du ficher dans le ldap"
ldapmodify -Y EXTERNAL -H ldapi:/// -Q -f modif-rootpw.ldif 
modifying entry "olcDatabase={2}mdb,cn=config"
```

Pour verifier la configuration
```bash
ldapwhoami -H ldap:// -D cn=Manager,dc=my-domain,dc=com -w discret
ldapsearch -H ldap:// -D cn=Manager,dc=my-domain,dc=com -w discret -b dc=my-domain,dc=com
# extended LDIF
#
# LDAPv3
# base <dc=my-domain,dc=com> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# search result
search: 2
result: 32 No such object

# numResponses: 1
```
!!! note
    
    Pour faire des modification dans les fichier a chaud on peut utiliser ```ldapvi```
    Pour choisir son editeur ```export EDITOR=/bin/nano```

    Pour le rendre en permanant le metre dans bash.rc

    ```echo "export EDITOR=/bin/nano" >> ~/.bashrc```

```bash title="Pour accèder au fichier de conf"
ldapvi -Y EXTERNAL -h ldapi:/// -b "olcDatabase={2}mdb,cn=config"
```


Pour se connecter au serveur en ldap 
```slappasswd```
Generer le hachage du mot de passe
```bash
slappasswd -s discret
echo  -n "discret" > .ldappass
```
Et changer la ligne avec le ssha dans la conf ldap
```
olcRootPW: {SSHA}Wd3pd4oWbAzxRjlqYry0AGBNyWRgSu0M
```
Les options de ldap se trouve dans */etc/openldap/ldap.conf*
```conf
BASE    dc=dom1,dc=local
URI     ldap://127.0.0.1
```
cela permet de ne pu definir s'est options lors de la connexion
```
ldapwhoami -D cn=admin,dc=dom1,dc=local -y ~/.ldappass
```

## Gestion de l'annuaire 
### Création de l'arboresance 

Créer l'arborescence suivante :

1. organisation "Mon domaine domX.local" : une entrée de classe "organization"
2.     |---> ou : "users" : une entrée d'annuaire basée sur la classe "organizationalUnit"
       |---> ou : "groups"
3.     |--------> une entrée d'annuaire basée sur la classe d'objet posixAccount : "ldapuser1"
4.     |--------> un posixGroup : "ldapgroupe1"


1. Création du ficher de création de l'organistaion 


```conf
dn: dc=dom1,dc=local
o: Mon domaine dom1.local
objectClass: organization
objectClass: dcObject
```

```bash title="envoie du fichier dans ldap"
ldapadd -D cn=admin,dc=dom1,dc=local -y ~/.ldappass -f org.ldif
```

1. 

```conf title="creation des OU"
dn: ou=users,dc=dom1,dc=local
objectClass: organizationalUnit

dn: ou=groups,dc=dom1,dc=local
objectClass: organizationalUnit
```

3. Création du groupes

!!! warning 
    
    De base le seul schmema qui est charger est ```core.schema```

Pour charger un schema
```
ldapadd -Y EXTERNAL -H ldapi:///  -f /etc/openldap/schema/nis.ldif
```

!!! Note "Besoin pour posixGroups"

    nis.schema,cosin.shema

Pour chercher les classe dans les schema 
```
grep -rni posixgroup /etc/openldap/schema/*.schema
sed  -n 175,185p  /etc/openldap/schema/nis.schema
```

```conf title="creation du groupe"
dn: cn=ldapgroupe1,ou=groups,dc=dom1,dc=local
gidNumber: 10001
objectClass: posixGroup
```

```conf title="creation du user" 
dn: cn=user1,ou=users,dc=dom1,dc=local
cn: user un
sn: un
gn: user
uid: userun
uidNumber: 10001
gidNumber: 10001
homeDirectory: /home/user1
objectClass: posixAccount
objectClass: inetOrgPerson
```

### PAM 

[SupportPAM](https://cl.actilis.net/index.php/s/frydpMr6wT8CQGN?dir=undefined&openfile=4379)

!!! info DEF

   ** Pluggable Authentication Modules (PAM)** est une création de Sun Microsystems et est supporté en 2006 sur les architectures Solaris, Linux, FreeBSD, NetBSD, AIX et HP-UX. L'administrateur système peut alors définir une stratégie d'authentification sans devoir recompiler des programmes d'authentification. PAM permet de contrôler la manière dont les modules sont enfichés dans les programmes en modifiant un fichier de configuration.

Les rêgles d'autentifications sont lister dans ```/etc/pam.d/```

![PAM_SCHEMA](./Images/schemapam.png)

Le chemin d'authentification est defini dans les fichier present dans *pam.d*
- required 
  - Il est nessesaire qu'elle renvoie un succès 
- requisite 
- sufficent 
  - Si elle renvoie un succès on arrete les controle **(=done)**

Les startegie sont écrits comme :

\[code_retour=action;default=action\]

Si un action est un chiffre c'est le nombre de controle qui vas skipper

!!! Note

    PAM ldap est en train d'être remplacer par **SSSD** 

### SSSD

Pour utiliser sssh on vas installer les paquets suivant :
> sssd sssd-ldap sssd-tools oddjob oddjob-mkhomedir

Création du repertoir à la premiere connexion
```
systemctl enable --now oddjobd
```

changement de la methode d'authentification avac **SSSD**
``` 
authselect select --force sssd with-mkhomedir
```

De base SSSD à une arboressance qui ne correspond pas à la notre 

Si on ne veux pas changer l'aboressance on peux changer la config de sssd dans ```/etc/sssd/sssd.conf```

!!! Abstract "Conseil"

    La plupart des logiciels/application qui se branche à un LDAP utilise la même arboressance que SSSD 

```conf
[sssd]
services = nss, pam
domains = local
[nss]
filter_users = root
filter_groups = root
[domain/dom1.local]
cache_credentials = true
id_provider = ldap
auth_provider = ldap
ldap_uri = ldap://dom1.local
ldap_tls_reqcert = never
ldap_search_base = dc=dom1,dc=local
ldap_user_search_base = ou=users,dc=dom1,dc=local
ldap_group_search_base = ou=groups,dc=dom1,dc=local
[pam]
offline_credentials_expiration = 1
offline_failed_login_attempts = 3
offline_failed_login_delay = 5
```
!!! info

    Le fichier doit etre en mod 600


Pour vérifier 
```bash
sssctl user-checks userun
```

### Certificats

Sur le routeur 

> Il nous le paquet openldap

Génération de aurtorité de certification 
```
openssl req \
-newkey rsa:4096 -nodes -keyout /etc/openldap/certs/ca-key.pem \
-subj '/countryName=FR/stateOrProvinceName=Bretagne/organizationName=Actilis/CN=CA-LDAP/' \
-x509 -sha256 -days 365 \
-extensions v3_ca \
-out /etc/openldap/certs/ca.pem
chmod 444 /etc/openldap/certs/ca.pem
```
Génération des certificats

```
install -d -m 2750 -o root -g ldap /etc/openldap/certs
openssl req \
-newkey rsa:4096 -nodes -keyout /etc/openldap/certs/key.pem \
-subj '/CN=ldap.dom1.local/' \
-out server.csr
chmod 440 /etc/openldap/certs/key.pem
```

```
openssl x509 -req -days 365 -sha256 -in server.csr -out /etc/openldap/certs/cert.pem \
-CA /etc/openldap/certs/ca.pem -CAkey /etc/openldap/certs/ca-key.pem -CAcreateserial \
-extfile <(printf "basicConstraints = CA:FALSE\n subjectAltName=IP:127.0.0.1,DNS:ldap.dom1.local")
```

Importer la configuration des certificats dans LDAP
```conf title="tld.ldif"
dn: cn=config
changetype: modify
replace: olcTLSCACertificateFile
olcTLSCACertificateFile: /etc/openldap/certs/ca.pem
-
replace: olcTLSCertificateFile
olcTLSCertificateFile: /etc/openldap/certs/cert.pem
-
replace: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/openldap/certs/key.pem
```
```bash title"intergration de la configuration" 
ldapadd -D cn=admin,dc=dom1,dc=local -y ~/.ldappass -f tls.ldif -c
```

```conf title="activation du tls */etc/openldap/ldap.conf*"
TLS_CACERT      /etc/openldap/cert/ca.pem
TLS_REQCERT demand
```

ne pas oublier de changer le nom de dommain en ldaps dans ```/etc/openldap/ldap.conf```

Pour tester \
```ldapwhoami -D cn=admin,dc=dom1,dc=local -y ~/.ldappass```

!!! info 

    Pour debugger slapd (Service de ldap)*
    ```journalctl -u slapd | tail```
    Pour rejouter de la verbositer au log on peut ajouter 
    ```obcLogLevel stats``` dans

!!! warning 

    Faire attention que les fichiers ca.pem,cert.pem,key.pem apartien bien au groupe openldap

### SudoLDAP 
[DOC](https://www.sudo.ws/docs/man/1.8.17/sudoers.ldap.man/)

Implementation de sudo LDAP 
```conf
 dn: cn=SUDOers,dc=dom1,dc=local
 objectClass: organizationalUnit
 
 dn: cn=defaults,ou=SUDOers,dc=dom1,dc=local
 objectClass: top
 objectClass: sudoRole
 cn: defaults
 description: Default sudoOption's go here
 sudoOption: env_keep+=SSH_AUTH_SOCK
```

!!! Warning "Nous ne connaisson pas **objectClass: sudoRole**"

    ```bash
    rpm -ql sudo | grep ldap
    /etc/sudo-ldap.conf
    /usr/share/man/man5/sudo-ldap.conf.5.gz
    /usr/share/man/man5/sudoers.ldap.5.gz
    man sudoers.ldap.5
    ```

    Ajout du repo **fusiondirectory-schema2ldif-release.repo**
    ```
    [fusiondirectory-schema2ldif-release]
    name=Fusiondirectory Packages for CentOS 7
    baseurl=https://public.fusiondirectory.org/centos7-schema2ldif-release/RPMS
    enabled=1
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-FUSIONDIRECTORY
    ```
    Ajout de la clé gpg 
    ```
    gpg --keyserver keys.openpgp.org --recv-key 0xFE0FEAE5AC483A86
    gpg --export -a "FusionDirectory Packages Signing Key <contact@fusiondirectory.org>" > FD-archive-key
    cp FD-archive-key /etc/pki/rpm-gpg/RPM-GPG-KEY-FUSIONDIRECTORY
    rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-FUSIONDIRECTORY
    ```
    ```bash Creation du fichier .ldif avec le schema
    schema2ldif /usr/share/doc/sudo/schema.OpenLDAP > sudo-schema.ldif
    ```
    ```
    ldapadd -Y EXTERNAL -H ldapi:///   -f sudo-schema.ldif
    ```



```
dn: cn=%wheel,ou=SUDOers,dc=my-domain,dc=com
objectClass: top
objectClass: sudoRole
cn: %wheel
sudoUser: %wheel
sudoHost: ALL
sudoCommand: ALL
```
