# Contenurisation et pipeline

## Installation de docker 

Pour commancer on vas créer un systeme de fichier specialement pour docker pour éviter de surcharger le / 

Avec cela si un coteneur prend trop de place la machine démarera toujours 
```bash
lvcreate -L 3G -n rootvg/lvdocker
```
Et ensuite on doit formater la partition
```bash 
mkfs.ext4 /dev/rootvg/lvdocker
```
Et pour qu'il se monte au démarage de la machine on le rajoute dans le *fstab*
```bash
echo "/dev/rootvg/lvdocker /var/lib/docker ext4 defaults" >> /etc/fstab
mount -a
```
!!! note "Alma linux n'est pas suporter par le script get-docker" 

    Pour fixer ce sousis on ajoute à la main les repo de Centos9 sur la alma
    ```bash
    curl -s https://download.docker.com/linux/centos/docker-ce.repo > /etc/yum.repos.d/docker-ce.repo
    ```

!!! warning

    Docker Compose V1 est en fin de vie en juin 2023
    les commande ```docker-compose``` ne fonctionneront pu

On fait un test pour voir si docker est bien fonctionnel
```yaml title="compose de whoami"
services:
  app:
    image: traefik/whoami
    ports:
    - 8080:80
```
Une fois le conteneur lancer on peut recuper les information du conteneur sur une page web *@IP:8080*

Pour verifier la sytaxe d'un compose
```docker compose config app```
```yml
name: test
services:
  app:
    image: traefik/whoami
    networks:
      default: null
    ports:
    - mode: ingress
      target: 80
      published: "8080"
      protocol: tcp
networks:
  default:
    name: test_default
```

## Création d'un site de documentation sous docker

Avec la balise **build** on peut passer un *Dockerfile* en argument pour qu'il puisse build une image avant de l'utiliser 
Si l'image est déja build et que l'on veux la rebuild ``` docker compose up -d --build```

!!! tips "Pour entrer dans le conteneur"
    
    ```docker compose exec app bash```

On utilise make pour nous faire des raccourcis de commandes
Dans un fichier nommée Makefile on a mit :

```yaml
help:
	@grep '#' Makefile
serve:
	@docker container rm -f dev-serv || true
	docker compose -f dev-srv.yml up -d 	
build:
	docker compose build
deploy:
	docker compose up -d --build
```

Pour faire notre documentaion nous utilisons [Mkdocs-materials](https://squidfunk.github.io/mkdocs-material/)

Déployer avec docker 

Nous avons un conteneur de dev 
```yaml
#On est parti de cette commande
#@docker container run -it --publish 8000:80 --restart on-failure --name dev-serv --volume ./build:/docs registry.actilis.net/docker-images/mkdocs:latest serve -a 0.0.0.0:80  
#Pour faire ce fichier compose que nous appelons avec le makefile
services:
  dev-serv:
    image: registry.actilis.net/docker-images/mkdocs:latest
    ports: 
    - 8000:80 
    restart : on-failure
    volumes:
    - "./:/docs" 
    command : serve -a 0.0.0.0:80
```
### Push sur git
Nous allons mettre note projet sur github 
1. Mettre la clé rsa de la machine sur notre compte  
2. Initialisation du repo
```bash
git init
git add .
git commit -m "Initial commit"
git add github git@github.com:v-galves/Pipeline.git
git push -u github
```
3. Aussi sur gitlab
```bash
git remote add gitlab git@gitlab.com:cs2i-p16bgalves/gnu-linux/docker-pipeline.git
git push -u gitlab main
```
### Les Container Registry

!!! info "DEF"

    Un registre de conteneurs est un référentiel - ou une collection de référentiels - utilisé pour stocker et accéder aux images de conteneurs. Les registres de conteneurs peuvent prendre en charge le développement d'applications basées sur des conteneurs, souvent dans le cadre de processus DevOps. Les registres de conteneurs peuvent se connecter directement aux plateformes d'orchestration de conteneurs telles que Docker et Kubernetes.

Ensuit sur **gitlab** on veux push nos images docker 
1. Créer un *personal accès token* dans les parametre de notre comptre gitlab
 - avec les droits
 - write_repository
 - read_registry
 - write_registry

Cela nous donne ce token : *glpat-tsdzBXViFNVHDrWZxVLH*

Le token est un moyen de s'authentifier au près de Git{lab,hub} sans à avoir besoin de taper de mots de passe *(intersant dans le cadre du devops)*

Docker login registry.gitlab.com -u v.galves -p glpat-tsdzBXViFNVHDrWZxVLH
2. docker login registry.gitlab.com -u v.galves -p glpat-tsdzBXViFNVHDrWZxVLH
3. Il faut rejouter un nom à notre image dans **compose.yml**
```yml
services:
  app:
    image: registry.gitlab.com/cs2i-p16bgalves/gnu-linux/docker-pipeline
    build:
      context: .
      dockerfile: Dockerfile
    ports:
    - 8080:80
```
4. On peut donc push sur **gitlab**
```bash
docker compose build
docker login registry.gitlab.com -u v.galves -p glpat-tsdzBXViFNVHDrWZxVLH
docker compose push
docker logout
```

Avec Github : 
1. Générer un token
   1. account > settings > Developper settings > personal accès tokens > Tokens(classic) > Generate new token
   2. Ajouter les droits
      1.  ```write:packages```
   3. Il nous donne notre token "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"
   4. le lien de notre repo de package > ghcr.io/v-galves/Pipeline
   5. Push sur le github

```bash
docker compose build
docker login ghcr.io -u v.galves -p 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
docker compose push
docker logout
```

!!! warning 

    Il est fortement décoseillier de renter les tokens en claire dans la ligne de commande 
    l'argument ```--password-stdin < fichier_comprenant_le_token``` est recomendé

```yml title="ajout de variable dans le compose.yml"
services:
  app:
    image: ${IMAGE:-registry.gitlab.com/cs2i-p16bgalves/gnu-linux/docker-pipeline}:${VERSION:-latest}
    build:
      context: .
      dockerfile: Dockerfile
    ports:
    - 8080:80
```

## Pipeline gitlab 

On fait un ci dans un fichier ```.gitlab-ci.yml```
```yml
image: docker:stable
stages:         
  - build
  - push
  - deploy

Constuction:       
  stage: build
  script:
    - docker compose build

Push:   
  stage: push    
  script:
    - docker compose push

Deploiment:   
  stage: deploy    
  script:
    - echo "Un jour on deploie l'appli"
```

Mais pour faire des pipeline CI il nous faut des runners 

!!! info

    GitHub met a disposition des runners mais c'est pas sur que toutes nos étapes soit fait sur le même runners et il faut que le notre dêpot soit en public

On push notre fichier
```
 git add .
 git commit -m "Ajout du fichier ci de gitlab (surment non fonctionnel)"
 git push -u github main
```

### Runner avec gitlab 

!!! info "DEF"

    Le Runner va gérer vos jobs et les lancer automatiquement quand une branche sera envoyée sur le dépôt ou lorsqu'elle sera mergée, par exemple. Vous pouvez également lancer les jobs à la main ou changer complètement la configuration.


!!! warning

    Les runners ne possede pas le pugin compose il faut donc utiliser des commande docker simple 


#### Installation du runner

On suit la [docs de gitlab](https://docs.gitlab.com/runner/install/docker.html)
On vas le faire sous Docker 

Pour avoir notre token de de groupe 
- Dans le projet (ou groupe de projet) > settings > CI/CD > Runners 

Ensuite on peut remplir un fichier ```.env``` qui sera appeler par le docker compose 
```env
COMPOSE_PROJECT_NAME=Cours-pipeline 
GITLAB_SERVER_URL=https://gitlab.com
RUNNER_TOKEN=secret
```

Ensuite on execute le docker compose suivant
```yml title="By fmicaux" linenums="1"
version: '3.9'
volumes:
  config:
  home:
  cache:

services:
  runner:
    image: gitlab/gitlab-runner:${RUNNER_VERSION:-latest}
    restart: always
    environment:
      RUNNER_NAME: ${COMPOSE_PROJECT_NAME}
      API_URL: ${GITLAB_SERVER_URL:-https://gitlab.actilis.net}/api/v4
      CI_SERVER_URL: ${GITLAB_SERVER_URL}/ci
      REGISTRATION_TOKEN: ${RUNNER_TOKEN:-123456789}
      CONCURRENT: ${RUNNER_CONCURRENT:-1}
      CHECK_INTERVAL: ${RUNNER_CHECK_INTERVAL:-1}
      DOCKER_VOLUMES: /var/run/docker.sock:/var/run/docker.sock
    entrypoint: "bash"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - config:/etc/gitlab-runner
      - home:/home/gitlab-runner
      - cache:/cache
    healthcheck:
      test: "gitlab-runner verify --name ${COMPOSE_PROJECT_NAME} 2>&1 | grep -q alive"
      start_period: 10s
      interval: 10s
      timeout: 10s
      retries: 10
    command: |
      -c 'set -e
          printf "\\nSetting configuration...\\n"
          mkdir -p /etc/gitlab-runner
          echo -e " log_level = \"warning\"\n concurrent = $${CONCURRENT}\n check_interval = $${CHECK_INTERVAL}\n\n [session_server]\n session_timeout = 3600 " > /etc/gitlab-runner/config.toml

          printf "\\nRegistering runner...\\n"
          gitlab-runner register --non-interactive --executor docker --docker-image docker:dind --locked=false --docker-privileged --run-untagged=${RUN_UNTAGGED:-true} --tag-list=${RUNNER_TAG_LIST:-notag}

          printf "\\nRunning runner...\\n"
          gitlab-runner run --user=gitlab-runner --working-directory=/home/gitlab-runner'
```

En lui passant les variables d'environements
```bash
docker compose --env-file formation.env up -d
```

Si on veux lancer plusieur runners 
Il faut changer la variable
```conf
CONCURRENT: ${RUNNER_CONCURRENT:-1} > CONCURRENT: ${RUNNER_CONCURRENT:1}
```
Et ensuit pour lancer les conteneur 
```
docker compose --env-file formation.env up -d --scale runner=4
```

!!! info 
    
    Ne pas oublier d'activer de runner dans les settings du projet

### Creation d'un Pipeline

Dans un fichier nommée *.gitlab-ci.yml*
```yml
image: docker:stable
stages:         
  - prebuild
  - build
  #- push
  #- deploy

PreBuild:
  stage: prebuild
  script:
  - set # Liste toute les variable d'environnement qu'il connait 

Constuction:       
  stage: build
  script:
    - docker image build -t image:version .
```
Voici les variables rendue par gitlab
```conf linenums="1"
CI='true'
CI_API_V4_URL='https://gitlab.com/api/v4'
CI_BUILDS_DIR='/builds'
CI_BUILD_BEFORE_SHA='0000000000000000000000000000000000000000'
CI_BUILD_ID='3805539632'
CI_BUILD_NAME='PreBuild'
CI_BUILD_REF='295787fa733d16b79e4c5620a4f9bc854d1d4c0c'
CI_BUILD_REF_NAME='main'
CI_BUILD_REF_SLUG='main'
CI_BUILD_STAGE='prebuild'
CI_BUILD_TOKEN='[MASKED]'
CI_COMMIT_AUTHOR='vgalves <vgalves@alma9-1-vg.local>'
CI_COMMIT_BEFORE_SHA='0000000000000000000000000000000000000000'
CI_COMMIT_BRANCH='main'
CI_COMMIT_DESCRIPTION=''
CI_COMMIT_MESSAGE='update pretask'
CI_COMMIT_REF_NAME='main'
CI_COMMIT_REF_PROTECTED='true'
CI_COMMIT_REF_SLUG='main'
CI_COMMIT_SHA='295787fa733d16b79e4c5620a4f9bc854d1d4c0c'
CI_COMMIT_SHORT_SHA='295787fa'
CI_COMMIT_TIMESTAMP='2023-02-21T10:08:02+01:00'
CI_COMMIT_TITLE='update pretask'
CI_CONCURRENT_ID='0'
CI_CONCURRENT_PROJECT_ID='0'
CI_CONFIG_PATH='.gitlab-ci.yml'
CI_DEFAULT_BRANCH='main'
CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX='gitlab.com:443/cs2i-p16bgalves/gnu-linux/dependency_proxy/containers'
CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX='gitlab.com:443/cs2i-p16bgalves/dependency_proxy/containers'
CI_DEPENDENCY_PROXY_PASSWORD='[MASKED]'
CI_DEPENDENCY_PROXY_SERVER='gitlab.com:443'
CI_DEPENDENCY_PROXY_USER='gitlab-ci-token'
CI_DISPOSABLE_ENVIRONMENT='true'
CI_JOB_ID='3805539632'
CI_JOB_IMAGE='docker:stable'
CI_JOB_JWT='[MASKED]'
CI_JOB_JWT_V1='[MASKED]'
CI_JOB_JWT_V2='[MASKED]'
CI_JOB_NAME='PreBuild'
CI_JOB_NAME_SLUG='prebuild'
CI_JOB_STAGE='prebuild'
CI_JOB_STARTED_AT='2023-02-21T10:07:49Z'
CI_JOB_STATUS='running'
CI_JOB_TIMEOUT='3600'
CI_JOB_TOKEN='[MASKED]'
CI_JOB_URL='https://gitlab.com/cs2i-p16bgalves/gnu-linux/docker-pipeline/-/jobs/3805539632'
CI_NODE_TOTAL='1'
CI_PAGES_DOMAIN='gitlab.io'
CI_PAGES_URL='https://cs2i-p16bgalves.gitlab.io/gnu-linux/docker-pipeline'
CI_PIPELINE_CREATED_AT='2023-02-21T10:07:48Z'
CI_PIPELINE_ID='784050179'
CI_PIPELINE_IID='6'
CI_PIPELINE_SOURCE='web'
CI_PIPELINE_URL='https://gitlab.com/cs2i-p16bgalves/gnu-linux/docker-pipeline/-/pipelines/784050179'
CI_PROJECT_CLASSIFICATION_LABEL=''
CI_PROJECT_DESCRIPTION=''
CI_PROJECT_DIR='/builds/cs2i-p16bgalves/gnu-linux/docker-pipeline'
CI_PROJECT_ID='43682405'
CI_PROJECT_NAME='docker-pipeline'
CI_PROJECT_NAMESPACE='cs2i-p16bgalves/gnu-linux'
CI_PROJECT_NAMESPACE_ID='60990254'
CI_PROJECT_PATH='cs2i-p16bgalves/gnu-linux/docker-pipeline'
CI_PROJECT_PATH_SLUG='cs2i-p16bgalves-gnu-linux-docker-pipeline'
CI_PROJECT_REPOSITORY_LANGUAGES='html,makefile,dockerfile'
CI_PROJECT_ROOT_NAMESPACE='cs2i-p16bgalves'
CI_PROJECT_TITLE='Docker-Pipeline'
CI_PROJECT_URL='https://gitlab.com/cs2i-p16bgalves/gnu-linux/docker-pipeline'
CI_PROJECT_VISIBILITY='private'
CI_REGISTRY='registry.gitlab.com'
CI_REGISTRY_IMAGE='registry.gitlab.com/cs2i-p16bgalves/gnu-linux/docker-pipeline'
CI_REGISTRY_PASSWORD='[MASKED]'
CI_REGISTRY_USER='gitlab-ci-token'
CI_REPOSITORY_URL='https://gitlab-ci-token:[MASKED]@gitlab.com/cs2i-p16bgalves/gnu-linux/docker-pipeline.git'
CI_RUNNER_DESCRIPTION='Cours-pipeline'
CI_RUNNER_EXECUTABLE_ARCH='linux/amd64'
CI_RUNNER_ID='21295886'
CI_RUNNER_REVISION='d540b510'
CI_RUNNER_SHORT_TOKEN='ByzFzGGy'
CI_RUNNER_TAGS='["notag"]'
CI_RUNNER_VERSION='15.9.1'
CI_SERVER='yes'
CI_SERVER_HOST='gitlab.com'
CI_SERVER_NAME='GitLab'
CI_SERVER_PORT='443'
CI_SERVER_PROTOCOL='https'
CI_SERVER_REVISION='96706e9cb1b'
CI_SERVER_TLS_CA_FILE='/builds/cs2i-p16bgalves/gnu-linux/docker-pipeline.tmp/CI_SERVER_TLS_CA_FILE'
CI_SERVER_URL='https://gitlab.com'
CI_SERVER_VERSION='15.9.0-pre'
CI_SERVER_VERSION_MAJOR='15'
CI_SERVER_VERSION_MINOR='9'
CI_SERVER_VERSION_PATCH='0'
CI_TEMPLATE_REGISTRY_HOST='registry.gitlab.com'
DOCKER_HOST='unix:///var/run/docker.sock'
DOCKER_TLS_CERTDIR='/certs'
DOCKER_VERSION='19.03.14'
FF_CMD_DISABLE_DELAYED_ERROR_LEVEL_EXPANSION='false'
FF_DISABLE_POWERSHELL_STDIN='false'
FF_DISABLE_UMASK_FOR_DOCKER_EXECUTOR='false'
FF_ENABLE_BASH_EXIT_CODE_CHECK='false'
FF_ENABLE_JOB_CLEANUP='false'
FF_KUBERNETES_HONOR_ENTRYPOINT='false'
FF_NETWORK_PER_BUILD='false'
FF_POSIXLY_CORRECT_ESCAPES='false'
FF_RESOLVE_FULL_TLS_CHAIN='true'
FF_SCRIPT_SECTIONS='false'
FF_SKIP_NOOP_BUILD_STAGES='true'
FF_USE_DIRECT_DOWNLOAD='true'
FF_USE_DYNAMIC_TRACE_FORCE_SEND_INTERVAL='false'
FF_USE_FASTZIP='false'
FF_USE_IMPROVED_URL_MASKING='false'
FF_USE_LEGACY_KUBERNETES_EXECUTION_STRATEGY='false'
FF_USE_NEW_BASH_EVAL_STRATEGY='false'
FF_USE_NEW_SHELL_ESCAPE='false'
FF_USE_POWERSHELL_PATH_RESOLVER='false'
FF_USE_WINDOWS_LEGACY_PROCESS_STRATEGY='true'
GITLAB_CI='true'
GITLAB_FEATURES='elastic_search,ldap_group_sync,multiple_ldap_servers,seat_link,usage_quotas,zoekt_code_search,repository_size_limit,admin_audit_log,auditor_user,custom_file_templates,custom_project_templates,db_load_balancing,default_branch_protection_restriction_in_groups,extended_audit_events,external_authorization_service_api_management,geo,instance_level_scim,ldap_group_sync_filter,object_storage,pages_size_limit,password_complexity,project_aliases,enterprise_templates,git_abuse_rate_limit,required_ci_templates,runner_maintenance_note,runner_performance_insights,runner_upgrade_management,runner_jobs_statistics'
GITLAB_USER_EMAIL='v.galves@cs2i-lorient.fr'
GITLAB_USER_ID='11237792'
GITLAB_USER_LOGIN='v.galves'
GITLAB_USER_NAME='VALENTIN GALVES'
HOME='/root'
HOSTNAME='runner-byzfzggy-project-43682405-concurrent-0'
IFS=' 	
'
LINENO=''
OLDPWD='/'
OPTIND='1'
PATH='/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
PPID='0'
PS1='\w \$ '
PS2='> '
PS4='+ '
PWD='/builds/cs2i-p16bgalves/gnu-linux/docker-pipeline'
RUNNER_TEMP_PROJECT_DIR='/builds/cs2i-p16bgalves/gnu-linux/docker-pipeline.tmp'
SHLVL='3'
```
[La doc de gitlab sur les variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

On peut donc en rajouter à notre gitlab-ci

Si on met des tags sur notre projet git on peut utiliser les tag de version du projet pour faire des versions des images 
```yml
image: docker:stable
variables:
  IMAGE_NAME: ${CI_REGISTRY}/${CI_PROJECT_PATH} 
stages:         
  - prebuild
  - build
  #- push
  #- deploy

PreBuild:
  stage: prebuild
  script:
  - set # Liste toute les variable d'environnement qu'il connait 

Constuction:       
  stage: build
  script:
    - docker image build -t ${IMAGE_NAME}:build-temp . #On build notre image 
    - docker image tag ${IMAGE_NAME}:build-temp ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel} # Si le git possede un tag il sera automatiquement aujouter sinon son tag sera devel 
    - echo ${CI_REGISTRY_PASSWORD} | docker login ${CI_REGISTRY} -u ${CI_REGISTRY_USER} --password-stdin # Grace au variable github on peut se log avec le runner 
    - docker image push                              ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel} # On 
    - docker logout
  after_script:
    - docker image rm       ${IMAGE_NAME}:build-temp  ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel}

```

```bash
 git add .
 git commit -m "Add docker login"
 git push gitlab
 git tag -a 0.0.1 -m "Normalement fonctionnel"
 git push gitlab --tag
 ```

 On passe la phase de push dans un autre étape pour faire propre
 ```yml
 image: docker:stable
variables:
  IMAGE_NAME: ${CI_REGISTRY}/${CI_PROJECT_PATH} 
stages:         
  #- prebuild
  - build
  - push
  #- deploy

Constuction:
  before_script:
  - set
  stage: build
  script:
    - docker image build -t ${IMAGE_NAME}:build-temp .
 
Publication de l'image:   
  stage: push    
  script:
    - docker image tag ${IMAGE_NAME}:build-temp ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel}
    - echo ${CI_REGISTRY_PASSWORD} | docker login ${CI_REGISTRY} -u ${CI_REGISTRY_USER} --password-stdin 
    - docker image push                              ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel}
  after_script:
    - docker image rm       ${IMAGE_NAME}:build-temp  ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel}

# Deploiment:   
#   stage: deploy    
#   script:
#     - echo "Un jour on deploie l'appli"
```
 
!!! info 
    Par defaut si un étape fail cela arret le pipelin

On rajoute un scan de vunerabilités sur notre image avent de la push

```yaml linenums="1"
image: docker:stable
variables:
  IMAGE_NAME: ${CI_REGISTRY}/${CI_PROJECT_PATH}
stages:
  #- prebuild
  - build
  - scan-vuln
  - push
  #- deploy

Constuction:
  before_script:
    - set
  stage: build
  script:
    - docker image build -t ${IMAGE_NAME}:build-temp .

Scan de l'image:
  stage: scan-vuln
  script:  
      - mkdir -p -m 2770 scan-result
      - docker container run --rm -v /var/run/docker.sock:/var/run/docker.sock -v trivy-cache:/root/.cache/  aquasec/trivy --cache-dir /root/.cache/  image --no-progress --scanners vuln ${IMAGE_NAME}:build-temp | tee scan-result/scan-${CI_PROJECT_NAME}.log
      - | 
        grep -q "CRITICAL: [^0]" scan-result/scan-${CI_PROJECT_NAME}.log && if [ ${STOP_IF_VULNERABILITY_FOUND:-0} != 0 ] ; then echo "Vulnérabilité CRITICAL détectée, arrêt du pipeline" && exit 1 ; fi ; true
    
  artifacts:
    when: always
    paths:
      - scan-result
    expire_in: 1 week

Publication de l'image:
  stage: push
  script:
    - docker image tag ${IMAGE_NAME}:build-temp ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel}
    - echo ${CI_REGISTRY_PASSWORD} | docker login ${CI_REGISTRY} -u ${CI_REGISTRY_USER} --password-stdin
    - docker image push                              ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel}
  after_script:
    - docker image rm       ${IMAGE_NAME}:build-temp  ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel}


# Deploiment:
#   stage: deploy
#   script:
#     - echo "Un jour on deploie l'appli"

# Deploiment:
#   stage: deploy
#   script:
#     - echo "Un jour on deploie l'appli"
```

!!! abstracte "La commande ```tee```"

    La commande tee permet de mettre de rediriger la sortie standard dans 


## Pipeline github 
### Création de runners github

git clone https://github.com/tcardonne/docker-github-runner.git .

chopper un token 
account > settings > Developper settings > personal accès tokens > Tokens(classic) > Generate new token
avec les droits suivants 
-  write:packages
- read:packages 
- delete:packages

Mettre le token dans un fichier d'environement ```.env```
```conf
RUNNER_REPOSITORY_URL=https://github.com/v-galves/Pipeline
GITHUB_ACCESS_TOKEN=secret
```

Modification du Makefile
```conf
compose:
        docker compose up -d --scale runner=4
        sleep 1
        docker compose ps
```

!!! abstract "docker compose ps"
    Cela permet de lister les coneneur present pesent pour note compose ici present

Ensuite on lance le build 
```bash
make build
``` 
Lancer le runner 
```
make compose 
```
### Création de pipeline 

Dans le projet faire un fichier 
```.github/workflows/main.yml```

```yml
name: test-action-1
on: push
jobs:
  build:
  
    runs-on: self-hosted #Permet de choisir le type de runners (ici self hosted)
    steps:
    - uses: actions/checkout@v3 #Permet de choisir le type de commande (V2 deprecated)
    - name: Write a multi-line message
      run: |
        echo This demo file shows a 
        echo very basic and easy-to
```

Voici les variable d'nvironement Github 
```conf linenums="1"
AGENT_TOOLSDIRECTORY=/opt/hostedtoolcache
BASH=/bin/bash
BASHOPTS=checkwinsize:cmdhist:complete_fullquote:extquote:force_fignore:globasciiranges:hostcomplete:interactive_comments:progcomp:promptvars:sourcepath
BASH_ALIASES=()
BASH_ARGC=()
BASH_ARGV=()
BASH_CMDS=()
BASH_LINENO=([0]="0")
BASH_SOURCE=([0]="/home/runner/_work/_temp/f4925310-6741-4bee-a366-28dcbd68e221.sh")
BASH_VERSINFO=([0]="5" [1]="0" [2]="3" [3]="1" [4]="release" [5]="x86_64-pc-linux-gnu")
BASH_VERSION='5.0.3(1)-release'
CI=true
DIRSTACK=()
EUID=0
GITHUB_ACCESS_TOKEN=***
GITHUB_ACTION=__run
GITHUB_ACTIONS=true
GITHUB_ACTION_REF=
GITHUB_ACTION_REPOSITORY=
GITHUB_ACTOR=v-galves
GITHUB_ACTOR_ID=122802527
GITHUB_API_URL=https://api.github.com
GITHUB_BASE_REF=
GITHUB_ENV=/home/runner/_work/_temp/_runner_file_commands/set_env_b227f7e2-a7da-431a-9744-51a85acf247c
GITHUB_EVENT_NAME=push
GITHUB_EVENT_PATH=/home/runner/_work/_temp/_github_workflow/event.json
GITHUB_GRAPHQL_URL=https://api.github.com/graphql
GITHUB_HEAD_REF=
GITHUB_JOB=build
GITHUB_OUTPUT=/home/runner/_work/_temp/_runner_file_commands/set_output_b227f7e2-a7da-431a-9744-51a85acf247c
GITHUB_PATH=/home/runner/_work/_temp/_runner_file_commands/add_path_b227f7e2-a7da-431a-9744-51a85acf247c
GITHUB_REF=refs/heads/main
GITHUB_REF_NAME=main
GITHUB_REF_PROTECTED=false
GITHUB_REF_TYPE=branch
GITHUB_REPOSITORY=v-galves/Pipeline
GITHUB_REPOSITORY_ID=604165360
GITHUB_REPOSITORY_OWNER=v-galves
GITHUB_REPOSITORY_OWNER_ID=122802527
GITHUB_RETENTION_DAYS=90
GITHUB_RUN_ATTEMPT=1
GITHUB_RUN_ID=4234344312
GITHUB_RUN_NUMBER=1
GITHUB_SERVER_URL=https://github.com
GITHUB_SHA=3ac93c3c19a4136e0f3a86bfff8620fa597c2e15
GITHUB_STATE=/home/runner/_work/_temp/_runner_file_commands/save_state_b227f7e2-a7da-431a-9744-51a85acf247c
GITHUB_STEP_SUMMARY=/home/runner/_work/_temp/_runner_file_commands/step_summary_b227f7e2-a7da-431a-9744-51a85acf247c
GITHUB_TRIGGERING_ACTOR=v-galves
GITHUB_WORKFLOW=test-action-1
GITHUB_WORKFLOW_REF=v-galves/Pipeline/.github/workflows/main.yml@refs/heads/main
GITHUB_WORKFLOW_SHA=3ac93c3c19a4136e0f3a86bfff8620fa597c2e15
GITHUB_WORKSPACE=/home/runner/_work/Pipeline/Pipeline
GROUPS=()
HOME=/root
HOSTNAME=704a0c5bdab6
HOSTTYPE=x86_64
IFS=$' \t\n'
MACHTYPE=x86_64-pc-linux-gnu
OPTERR=1
OPTIND=1
OSTYPE=linux-gnu
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PPID=148
PS4='+ '
***
RUNNER_ALLOW_RUNASROOT=true
RUNNER_ARCH=X64
RUNNER_LABELS=
RUNNER_NAME=my-runner
RUNNER_OS=Linux
RUNNER_REPLACE_EXISTING=true
RUNNER_REPOSITORY_URL=https://github.com/v-galves/Pipeline
RUNNER_TEMP=/home/runner/_work/_temp
RUNNER_TOKEN=A5I5CXZLUIPHKSYSTDJZU4TD6TZJA
RUNNER_TOOL_CACHE=/opt/hostedtoolcache
RUNNER_TRACKING_ID=github_2a762ba4-8ad2-4361-ac6e-62beb320873f
RUNNER_WORKSPACE=/home/runner/_work/Pipeline
RUNNER_WORK_DIRECTORY=_work
SHELL=/bin/bash
SHELLOPTS=braceexpand:errexit:hashall:interactive-comments
SHLVL=1
SUPERVISOR_ENABLED=1
SUPERVISOR_GROUP_NAME=runner
SUPERVISOR_PROCESS_NAME=runner
TERM=dumb
UID=0
_=./externals/node16/bin/node
```



Enusite il faut juste push sur gitHUB

On vas essayer de faire le meme ficher ci que sur github

```yml title="Fait par ChatGPT" linenums="1" 

name: CI/CD Pipeline

on:
  push:
    branches:
      - main
  pull_request:

env:
  IMAGE_NAME: ${{secrets.DOCKER_REGISTRY}}/${{github.repository}}

jobs:
  build:
    runs-on: ubuntu-latest
    steps:
      - name: Checkout repository
        uses: actions/checkout@v2
      - name: Build Docker image
        run: docker image build -t $IMAGE_NAME:build-temp .
      - name: Save Docker image
        run: docker save $IMAGE_NAME:build-temp | gzip > image.tar.gz
      - name: Upload Docker image artifact
        uses: actions/upload-artifact@v2
        with:
          name: image
          path: image.tar.gz

  scan-vuln:
    runs-on: ubuntu-latest
    needs: build
    steps:
      - name: Create scan result directory
        run: mkdir -p -m 2770 scan-result
      - name: Load Docker image artifact
        uses: actions/download-artifact@v2
        with:
          name: image
      - name: Scan Docker image
        run: |
          docker load < image.tar.gz
          docker container run --rm -v /var/run/docker.sock:/var/run/docker.sock -v trivy-cache:/root/.cache/  aquasec/trivy --cache-dir /root/.cache/  image --no-progress --scanners vuln $IMAGE_NAME:build-temp | tee scan-result/scan-${{github.event.pull_request.head.repo.name}}.log
          if grep -q "CRITICAL: [^0]" scan-result/scan-${{github.event.pull_request.head.repo.name}}.log && [ "${{env.STOP_IF_VULNERABILITY_FOUND:-0}}" != 0 ]; then echo "Vulnérabilité CRITICAL détectée, arrêt du pipeline" && exit 1; fi

      - name: Save scan result artifact
        uses: actions/upload-artifact@v2
        with:
          name: scan-result
          path: scan-result
        if: always()

  push:
    runs-on: ubuntu-latest
    needs: scan-vuln
    steps:
      - name: Load Docker image artifact
        uses: actions/download-artifact@v2
        with:
          name: image
      - name: Docker login
        run: echo ${{secrets.DOCKER_PASSWORD}} | docker login -u ${{secrets.DOCKER_USERNAME}} --password-stdin ${{secrets.DOCKER_REGISTRY}}
      - name: Push Docker image
        run: |
          docker load < image.tar.gz
          docker image tag $IMAGE_NAME:build-temp $IMAGE_NAME:${{github.ref}}
          docker image push $IMAGE_NAME:${{github.ref}}
      - name: Remove temporary Docker images
        run: |
          docker image rm $IMAGE_NAME:build-temp $IMAGE_NAME:${{github.ref}}
```

## Ingress Controller

[Cours](https://cl.actilis.net/index.php/s/frydpMr6wT8CQGN?dir=undefined&path=%2F03%20-%20Docker%20%26%20Pipelines%20CICD&openfile=8436)

!!! info "DEF"

    Traefik (prononcer trafic) est un reverse proxy HTTP moderne et un répartiteur de charge qui facilite le déploiement de microservices. Traefik s'intègre à vos composants d'infrastructure existants ( Docker, Swarm mode, Kubernetes, Marathon, Consul, Etcd, Rancher, Amazon ECS, ...) et se configure automatiquement et dynamiquement.

  !!! note 

      Dans traefik il existe plusieur type de plusieurs type de tag 
      - Un routeur est chargé de connecter les demandes entrantes aux services qui peuvent les traiter. Au cours de ce processus, les routeurs peuvent utiliser des éléments d'intergiciel pour mettre à jour la demande, ou agir avant de transmettre la demande au service.
        - Un middleware est attachés aux routeurs, les intergiciels permettent d'ajuster les demandes avant qu'elles ne soient envoyées à votre service (ou avant que les réponses des services ne soient envoyées aux clients).

Treafik fonctionne avec des étiquette une fois que le traefik est en route c'est les conteneurs qui vont venir se présenter a traefik.
Traefik écoute sur la socket docker directement

Premier compose avec traefik
```yaml linenums="1"
networks:
  rp-net:
    name: traefik-net
services:
  proxy:
    image: traefik
    command: |-
      --providers.docker
      --log.level=debug
      --entryPoints.http.address=:80
    restart: always
    networks:
    - rp-net
    ports:
    - "80:80"
    volumes:
      - type: bind
        source: /var/run/docker.sock
        target: /var/run/docker.sock

  whoami:
    image: traefik/whoami
    networks:
    - rp-net
    labels:
    - traefik.http.routers.whoami.rule=Path(`/whoami`)
```

```bash title="Pour tester" hl_lines="1"
curl http://127.0.0.1/whoami
Hostname: fb92e6cc44ea
IP: 127.0.0.1
IP: 172.21.0.2
RemoteAddr: 172.21.0.3:47762
GET /whoami HTTP/1.1
Host: 127.0.0.1
User-Agent: curl/7.76.1
Accept: */*
Accept-Encoding: gzip
X-Forwarded-For: 172.21.0.1
X-Forwarded-Host: 127.0.0.1
X-Forwarded-Port: 80
X-Forwarded-Proto: http
X-Forwarded-Server: b6d39befab6b
X-Real-Ip: 172.21.0.1
```

Si on veux faire le routeur par le nom d'hote 
```yaml
labels:
    - traefik.http.routers.whoami.rule=Host(`whoami.alma9-1-vg.local`)
```
```bash title="Pour tester"
curl -H "Host: whoami.alma9-1-vg.local" http://127.0.0.1
Hostname: 62b9de2c70b5
IP: 127.0.0.1
IP: 172.21.0.2
RemoteAddr: 172.21.0.3:47962
GET / HTTP/1.1
Host: whoami.alma9-1-vg.local
User-Agent: curl/7.76.1
Accept: */*
Accept-Encoding: gzip
X-Forwarded-For: 172.21.0.1
X-Forwarded-Host: whoami.alma9-1-vg.local
X-Forwarded-Port: 80
X-Forwarded-Proto: http
X-Forwarded-Server: b6d39befab6b
X-Real-Ip: 172.21.0.1
```

On vas rajouter un Dashboard sur Traefik 
```yaml linenums="1" hl_lines="7 17-23"
  proxy:
    image: traefik
    command: |-
      --providers.docker
      --log.level=debug
      --entryPoints.http.address=:80
      --api.insecure=true
    restart: on-failure
    networks:
    - rp-net
    ports:
    - "80:80"
    volumes:
      - type: bind
        source: /var/run/docker.sock
        target: /var/run/docker.sock
    labels:
    - "traefik.http.routers.dashboard.rule=PathPrefix(`/traefik`) || PathPrefix(`/api`)"
    - "traefik.http.routers.dashboard.service=api@internal"
    # On soumet ce routeur là à des middlewares: strip du prefix
    - "traefik.http.routers.dashboard.middlewares=dashboard-stripprefix"
    # On implémente les middlewares en question
    - "traefik.http.middlewares.dashboard-stripprefix.stripprefix.prefixes=/traefik"
```
On veut mettre un mot de passe sur notre dashboard

Ici notre mot de passe sera 
user:CS2I
```bash
echo CS2I | docker container run --rm -i httpd htpasswd -ni user
user:$apr1$UtcNaSP6$0QsCaeMEZ1FvKocs6xLBd.
```
!!! Warning 
    Ne pas prendre le **.** la fin du retour  


Ensuite on peut l'ajouter dans notre configuration **traefik**
```yml linenums="1" hl_lines="28-29"
proxy:
  image: traefik
  command: |-
    --providers.docker
    --log.level=debug
    --entryPoints.http.address=:80
    --api.insecure=true
    --provider.docker.exposedbydefault="False"
  restart: on-failure
  networks:
  - rp-net
  ports:
  - "80:80"
  volumes:
    - type: bind
      source: /var/run/docker.sock
      target: /var/run/docker.sock
  labels:
  - traefik.enable=true
  - "traefik.http.routers.dashboard.rule=PathPrefix(`/traefik`) || PathPrefix(`/api`)"
  - "traefik.http.routers.dashboard.service=api@internal"
  # On soumet ce routeur là à des middlewares: strip du prefix
  - "traefik.http.routers.dashboard.middlewares=dashboard-stripprefix"
  # On implémente les middlewares en question
  - "traefik.http.middlewares.dashboard-stripprefix.stripprefix.prefixes=/traefik"
  # On ajoute un mot de passe a notre interface 
  - "traefik.http.routers.dashboard.middlewares=auth-dashboard,dashboard-stripprefix"
  - "traefik.http.middlewares.auth-dashboard.basicauth.users=user:$$apr1$$UtcNaSP6$$0QsCaeMEZ1FvKocs6xLBd"
```
!!! warning 

     En yaml il faut **DOUBLER** les **$**

!!! note 

      La commande ```--provider.docker.exposedbydefault="False"``` perment de pas publier tous les conteneur vu dans la docker.sock
      Donc pour les exposer il faut ajouter un label ```traefik.enable=true```


### Gitlab CD 

On veux deployer automatiquement de notre conteneur docker sur un machine de prod
!!! note "Le runner"
    Le runner doit tourner sur la machine de prod 

```yaml linenums="1" title="compose.yml"
name: vgalves
services:
  whoami:
    image: traefik/whoami
    networks:
    - traefik-net
    labels:
        - traefik.http.routers.vgalves-whoami.rule=Host(`vgalves.cs2i.actilis.net`)
        - "traefik.enable=true"
        #- "traefik.http.services.docsrv.loadbalancer.server.port=80"
networks:
  traefik-net:
    external: true
```

```yaml linenums="1" title=".gitliab-ci" 
image: docker:latest
variables:
  PROD_SERVER_USER: vgalves
  PROD_SERVER_IP: cs2i.actilis.net
stages:
  - deploiment 

Deploiment:
  stage: deploiment
  script:
    - docker compose up -d
```

!!! note "Fichier de configuration traefik"

    traefik possede deux proriété 

    - ```providers.directory``` sert a definir un repertoir ou il vas lire tous les fichier de conf en yaml ou en toml

    - ```providers.file``` sert à defini **un fichier** de conf en yaml ou en toml

    - Ses variables sont par defaut non definie

On vas faire un traefik en https
Ou pas 

On vas déployer un site de documentation en CD
```yaml title="Notre docker compose"
name: vgalves
services:
  app:
    image: ${IMAGE:-registry.gitlab.com/cs2ip16/valentin/srvdoc}:${VERSION:-latest}
    build:
      context: .
      dockerfile: Dockerfile
    # ports:
    # - 8080:80
    labels:
      - traefik.http.routers.vgalves-docsrv.rule=Host(`vgalves.cs2i.actilis.net`)
      - "traefik.enable=true"
```

```yaml linenums="1" title="1"
FROM registry.actilis.net/docker-images/mkdocs:latest as constructeur
#COPY .git .git
COPY mkdocs.yml /docs
COPY src        /docs/src
#COPY includes   /docs/includes
RUN mkdocs build


## Deuxième étape : Construire l'image basée sur nginx
FROM registry.actilis.net/docker-images/httpd:2.4-alpine
COPY --from=constructeur --chown=www-data /docs/site /var/www/html
``` 

```yaml linenums="1" title="Notre .gitlab-ci"
image: docker:latest
variables:
  IMAGE_NAME: ${CI_REGISTRY}/${CI_PROJECT_PATH}
stages:
  - build
  - scan-vuln
  - push
  - deploiment 

Constuction:
  before_script:
    - set
  stage: build
  script:
    - docker image build -t ${IMAGE_NAME}:build-temp .

Scan de l'image:
  stage: scan-vuln
  script:  
      - mkdir -p -m 2770 scan-result
      - docker container run --rm -v /var/run/docker.sock:/var/run/docker.sock -v trivy-cache:/root/.cache/  aquasec/trivy --cache-dir /root/.cache/  image --no-progress --scanners vuln ${IMAGE_NAME}:build-temp | tee scan-result/scan-${CI_PROJECT_NAME}.log
      - | 
        grep -q "CRITICAL: [^0]" scan-result/scan-${CI_PROJECT_NAME}.log && if [ ${STOP_IF_VULNERABILITY_FOUND:-0} != 0 ] ; then echo "Vulnérabilité CRITICAL détectée, arrêt du pipeline" && exit 1 ; fi ; true
    
  # artifacts:
  #   when: always
  #   paths:
  #     - scan-result
  #   expire_in: 1 week

Publication de l'image:
  stage: push
  script:
    - docker image tag ${IMAGE_NAME}:build-temp ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel}
    - echo ${CI_REGISTRY_PASSWORD} | docker login ${CI_REGISTRY} -u ${CI_REGISTRY_USER} --password-stdin
    - docker image push                              ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel}
  after_script:
    - docker image rm       ${IMAGE_NAME}:build-temp  ${IMAGE_NAME}:${CI_COMMIT_TAG:-devel}

Deploiment:
  stage: deploiment
  script:
    - docker compose up -d --remove-orphans
```
