# Installation automatisée de systeme Linux

## Ressources 
[lien du pad](https://pad.actilis.net/p/cs2i-admav)

Ip de la machine : **192.168.239.135**

[Lien du support de cour](https://cl.actilis.net/index.php/s/frydpMr6wT8CQGN)


### Kickstart redhat : 

La méthode d'installation Red Hat Kickstart est utilisée par Fedora, Red Hat Enterprise Linux et les distributions Linux associées pour effectuer automatiquement l'installation et la configuration du système d'exploitation sans surveillance.

Chez la famille ubuntu : **Préseed** 

Difference entre ext4 et xfs
- xfs plus rapide 
- ext4 peut est rétrecie


## Cahier des charge du master 

A/ RHEL/Stream 9 \
1. Réaliser une instalaltion manuellement sur une VM \
2. Récupérer son fichier /root/anaconda-ks.cfg --> /srv/ks.cfg  
3. Le mettre à disposition de clients HTTP : conteneur Docker
Mettre au point le fichier "ks.cfg" en focntion d'un cahier des charges

3 partitions :

1. /boot : 500Mo, format ext2
2. Swap : 1Go
3. Système de fichier racine (/), ext4, 3 Go, 
mais sur du LVM, volume groupe = rootvg, volume logique = rootlv

On se sert de la première VM comme serveur HTTP pour lancer l'installation des autres.

4/ Lancer une installation sur une autre VM utilisant ce KS. \
Au démarrage : inst.ks=http://........./ks.cfg \
si c'est pas tout à fait bon, on reboucle en 3.

Lors du premier lancement de la machine avec l'image iso : 

on peut fait **TAB** pour changer le lancement 
Si on veux automatiser l'instalation il faut rajouter 
```
inst.ks=http://@ipsource/ks.cf
inst.ks=http://192.168.239.135/ks.cfg
```

!!! warning
    Si la resolution ne permet pas d'accèder à toutes les vi options 
    changer la résolution en changant la variable de resolution au lancement

```
inst.resolution=1024x768
```

Reponse au questions
Clavier : FR
Pationnement
Partitionnement personnalisé

Une fois l'instalation terminée on peut retrouver le fichier resumant l'intallation dans /root

On veut mettre a disposition notre fichier ks.cfg
On le deplace dans srv/kickstart/ks.cfg

### Installation de docker
```
 curl -sSL https://get.docker.com | bash 
```

Pour vervifier si on a bien docker

` docker version `

Démarrer docker au lancement de la machine 

` systemctl enable --now docker.service `

### Donner accès a la ressource en http avec docker 

```
docker container run -d --name websrv --publish 80:80 --volume /srv/kickstart:/usr/local/apache2/htdocs  httpd 
```

Docker na pas acces à ks.cfg

```
chmod 644 /root/ks.cfg
```

## Cahier des charges
```

Le cahier des charges de nos installations kickstartées :
- même partitionnement que celui de la machine racine
- rejouable (idempotent)... attention au nettoyage des partitions à chaque tour
- la machine s'arrête en fin d'installation (qui se passe en mode texte)
- traitements de post-installation :
# 0a- "micro" est installé dans /usr/local/bin,
# 0b- et un lien symbolique existe pour le pointer depuis /usr/local/sbin
# 1- user n'a pas besoin de s'authentifier pour sudo ..... "NOPASSWD"
# 2- on peut se connecter en tant que "user" avec un cle publique definie dès ici.
```
!!! Tips 
    Générer un hash (sha512) :
        echo password | openssl passwd -6 -stdin

    Commenter la ligne "%wheel....." dans /etc/sudoers : 
        sur la ligne commençant par %wheel, remplacer le début par #-espace

    Décommenter la ligne "# %wheel...NOPASSWD..." dans /etc/sudoers : 
        sur la ligne commençant par #...et contenant NOPASSWD, remplacer par rien le #-espace du début

    ==> Les deux en une seule commande ?
    ```
    sed -e '/^%wheel/s,^,# ,' -e '/^# %wheel.*NOPASSWD/s,^# ,,' /etc/sudoers
    ```

## Changement du ficher

[ks.cfg](./ks.cfg)

``` title=".ks"
--8<-- "./.ks"
```

!!! tip "%post"
	Balise permetant d'executer des commandes en bash après l'installation de la machine 

!!! tip "--nochroot"
	permet de travailler hors de l'espace des partitions monter juste avant *Permet de ne pas senfermer dans le filesysteme de la machine*
	**Si le nochroot est mis il faut prefixer les liens de fichier par "/mnt/sysimage"** 

### Ajout de parametres 

Ajout d'un utilisateur et son mdp en sha256
```bash
user --groups=wheel --name=vgalves --password=$6$HuGj1KAfb3y/gjK1$00/HWvujY.RdFLPxkP0QgRWjPbFWOJghcitCucaAIwbLtNuE9SFhngX8wXE.4hsbOSoz0Hn6DVCoU6.p63seU/ --iscrypted --gecos="vgalves"
```

## Modifier un fichier iso

*Il nous manque de la place on en rajoute*
``` linenums="1"
lvresize --size +2G -r /dev/rootvg/rootlv 
#On crée des repertoires
mkdir /root/{new,orig}-iso
#Monter l'ISO d'origine dans un dossier : ex. /root/orig-iso
mount -o ro  /dev/sr0  /root/orig-iso
#Recopier tout son contenu vers un dossier de travail : ex. /root/new-iso
#On utilise rsync, qu'on installe avant : 
dnf -y install rsync
rsync -a /root/orig-iso/ /root/new-iso
#Démonter l'ISO d'origine, nous n'en avons plus besoin.
umount /root/orig-iso
```
5/ Construire une nouvelle image : new-iso.iso embarquant le dossier "/root/new-iso"
     ==> En se plaçant dans le dossier "new-iso"
```
mkisofs -r -T -J -V 'CentOS-Stream-9-x86_64-dvd' -o /srv/kickstart/CentOS-Stream-9-auto.iso -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table .
```

**Il faut installer mkisofs**

Faire nos modifications (dans /root/new-iso)
on a rajouter 
```
label linuxAuto
  menu label ^Autoinstall CentOS Stream 9
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=hd:LABEL=CentOS-Stream-9-BaseOS-x86_64 quiet inst.ks=http://192.168.239.135/ks.cfg
```

!!! warning 
    **Marche Pas**

Erreur ks 

![images_erreur_KS](Images/Image_erreur_ks.png)

Faire attention au format du fichier qu'il n'ajoute pas des retours chariot

!!! tip "Changer le nom d'une machine" 
    
    Avec la commande **Hostnamectl**

    ```hostnamectl set-hostname alma9-1-vg.local```
    